﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lawyer
{
    public partial class Prosecutor : Form
    {
        Database database = new Database();
        string prosecutorId = "0";

        public Prosecutor()
        {
            InitializeComponent();

            dataGridViewProsecutor.AutoGenerateColumns = false;
            BindProsecutorData();
        }

        private void BindProsecutorData()
        {
            SqlCommand sqlCmd = new SqlCommand("USP_GetProsecutorData");
            dataGridViewProsecutor.DataSource = database.GetDataTable(sqlCmd);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_InsertProsecutor");
            sqlCmd.Parameters.AddWithValue("@ProsecutorType", textBoxProsecutorType.Text);
            sqlCmd.Parameters.AddWithValue("@ProsecutorName", textBoxProsecutorName.Text);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกเพิ่มแล้ว.");

                BindProsecutorData();
            }
        }
        private void ClearForm()
        {
            this.prosecutorId = "0";
            textBoxProsecutorName.Text = "";

            btnAdd.Enabled = true;
            btnModify.Enabled = false;
            btnDelete.Enabled = false;
        }
        private void btnModify_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_UpdateProsecutor");
            sqlCmd.Parameters.AddWithValue("@ProsecutorId", this.prosecutorId);
            sqlCmd.Parameters.AddWithValue("@ProsecutorType", textBoxProsecutorType.Text);
            sqlCmd.Parameters.AddWithValue("@ProsecutorName", textBoxProsecutorName.Text);

            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกแก้ไขแล้ว.");

                ClearForm();

                BindProsecutorData();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_DeleteProsecutor");
            sqlCmd.Parameters.AddWithValue("@ProsecutorId", this.prosecutorId);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {

                MessageBox.Show("ข้อมูลถูกลบแล้ว.");

                ClearForm();

                BindProsecutorData();
            }
        }

        private void dataGridViewProsecutor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewProsecutor.Columns[e.ColumnIndex].Name == "Select")
            {
                this.prosecutorId = dataGridViewProsecutor.Rows[e.RowIndex].Cells["Id"].Value.ToString();

                textBoxProsecutorType.Text = dataGridViewProsecutor.Rows[e.RowIndex].Cells["ProsecutorType"].Value.ToString();
                textBoxProsecutorName.Text = dataGridViewProsecutor.Rows[e.RowIndex].Cells["ProsecutorName"].Value.ToString();

                btnAdd.Enabled = false;
                btnModify.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void dataGridViewProsecutor_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dataGridViewProsecutor.Rows[e.RowIndex].Cells["RecordNo"].Value = e.RowIndex + 1;
            }
        }
    }
}
