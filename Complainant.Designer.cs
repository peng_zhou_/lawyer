﻿namespace Lawyer
{
    partial class Complainant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewComplainant = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxComplainantName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Select = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ComplainantName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComplainant)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewComplainant
            // 
            this.dataGridViewComplainant.AllowUserToAddRows = false;
            this.dataGridViewComplainant.AllowUserToDeleteRows = false;
            this.dataGridViewComplainant.AllowUserToResizeRows = false;
            this.dataGridViewComplainant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewComplainant.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Select,
            this.Id,
            this.RecordNo,
            this.ComplainantName});
            this.dataGridViewComplainant.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewComplainant.Location = new System.Drawing.Point(8, 147);
            this.dataGridViewComplainant.Name = "dataGridViewComplainant";
            this.dataGridViewComplainant.Size = new System.Drawing.Size(509, 150);
            this.dataGridViewComplainant.TabIndex = 43;
            this.dataGridViewComplainant.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewComplainant_CellContentClick);
            this.dataGridViewComplainant.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewComplainant_CellFormatting);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.ForeColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(246, 79);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 41;
            this.btnDelete.Text = "ลบ";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.Enabled = false;
            this.btnModify.ForeColor = System.Drawing.Color.Blue;
            this.btnModify.Location = new System.Drawing.Point(164, 79);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 40;
            this.btnModify.Text = "แก้ไข";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAdd.Location = new System.Drawing.Point(83, 79);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 39;
            this.btnAdd.Text = "เพิ่ม";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gold;
            this.panel1.Location = new System.Drawing.Point(-169, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 48);
            this.panel1.TabIndex = 42;
            // 
            // textBoxComplainantName
            // 
            this.textBoxComplainantName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxComplainantName.Location = new System.Drawing.Point(58, 12);
            this.textBoxComplainantName.Name = "textBoxComplainantName";
            this.textBoxComplainantName.Size = new System.Drawing.Size(460, 22);
            this.textBoxComplainantName.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 16);
            this.label4.TabIndex = 37;
            this.label4.Text = "ผู้ร้อง";
            // 
            // Select
            // 
            this.Select.HeaderText = "เลือก";
            this.Select.Name = "Select";
            this.Select.Text = "เลือก";
            this.Select.ToolTipText = "เลือก";
            this.Select.UseColumnTextForLinkValue = true;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "ComplainantId";
            this.Id.HeaderText = "ComplainantId";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // RecordNo
            // 
            this.RecordNo.HeaderText = "No.";
            this.RecordNo.Name = "RecordNo";
            // 
            // ComplainantName
            // 
            this.ComplainantName.DataPropertyName = "ComplainantName";
            this.ComplainantName.HeaderText = "ผู้ร้อง";
            this.ComplainantName.Name = "ComplainantName";
            // 
            // Complainant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 393);
            this.Controls.Add(this.dataGridViewComplainant);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBoxComplainantName);
            this.Controls.Add(this.label4);
            this.Name = "Complainant";
            this.Text = "ผู้ร้อง";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComplainant)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewComplainant;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxComplainantName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewLinkColumn Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ComplainantName;
    }
}