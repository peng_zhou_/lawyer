﻿namespace Lawyer
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCompletedFileNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxCaseNo2 = new System.Windows.Forms.TextBox();
            this.textBoxCaseNo1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAccountNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxBlackCaseNo = new System.Windows.Forms.TextBox();
            this.textBoxRedCaseNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxCourtName = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxAllegationType = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxProsecutor = new System.Windows.Forms.ComboBox();
            this.comboBoxComplainant = new System.Windows.Forms.ComboBox();
            this.comboBoxDefendant = new System.Windows.Forms.ComboBox();
            this.textBoxDefendantRemark1 = new System.Windows.Forms.TextBox();
            this.textBoxDefendantRemark2 = new System.Windows.Forms.TextBox();
            this.textBoxDefendantRemark3 = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.comboBoxCaseOfficer = new System.Windows.Forms.ComboBox();
            this.comboBoxCaseGroup = new System.Windows.Forms.ComboBox();
            this.comboBoxBSY = new System.Windows.Forms.ComboBox();
            this.comboBoxMortgage = new System.Windows.Forms.ComboBox();
            this.comboBoxAssetDetectiveOfficer = new System.Windows.Forms.ComboBox();
            this.comboBoxWitness = new System.Windows.Forms.ComboBox();
            this.textBoxCaseWorkRel = new System.Windows.Forms.TextBox();
            this.textBoxOperator = new System.Windows.Forms.TextBox();
            this.textBoxFollowingResult = new System.Windows.Forms.TextBox();
            this.textBoxCallResult = new System.Windows.Forms.TextBox();
            this.textBoxCaseResult = new System.Windows.Forms.TextBox();
            this.textBoxAccountingOfficer = new System.Windows.Forms.TextBox();
            this.textBoxComplaintWorkRel = new System.Windows.Forms.TextBox();
            this.textBoxYellowResult = new System.Windows.Forms.TextBox();
            this.textBoxRemark = new System.Windows.Forms.TextBox();
            this.textBoxDefendantNameList = new System.Windows.Forms.TextBox();
            this.textBoxHelperLawyer = new System.Windows.Forms.TextBox();
            this.textBoxDisburse3 = new System.Windows.Forms.TextBox();
            this.textBoxDisburse2 = new System.Windows.Forms.TextBox();
            this.comboBoxAfterSentenceLawyer = new System.Windows.Forms.ComboBox();
            this.comboBoxDetectiveLaywer = new System.Windows.Forms.ComboBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.textBoxDisburse1 = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.comboBoxComplainantLawyer = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dateTimePickerPaymentWithdrawalDate3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPaymentWithdrawalDate2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPaymentWithdrawalDate1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerWithdrawalDividedAttachedFundsDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPermittedDate2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPermittedDate1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerWithdrawalDate1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerSubrogatedWithdrawalDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerWithdrawalDate2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerWithdrawalAttachment2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerExecutationAttachment2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerWithdrawalAttachment1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerExecutationAttachment1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerEndOfSellingAuctionDueDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerExpiredCaseDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAcquiringProxyDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerSendingProxyDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAbsoluteReceivershipDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAttachmentAfterSellingDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerRequestForPaymentDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDivisionDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerSubrogationDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerApplyingDivisionDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerSeizingPropertiesDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerApplyingAttachmentDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAcquiringSuccessorDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerOpeningCaseDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerReturningChequeDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerCourtChequeDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAppealJudgementDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAcceptingAppealJudgementDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerTerminatingCaseApprovalDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerWithdrawalDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerJudgementExecutationClosingDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDueDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerClaimDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerReturningFileDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerCourtFeeRefundRequestDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerLodgingAppealDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerApplyingPetitionDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickJudgementExecutationDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerIssuingWarrantDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerMandatoryDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDayOfJudgment = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerFilingDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerNoticeDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAdmissionDate = new System.Windows.Forms.DateTimePicker();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.comboBoxUnnameDropDown3 = new System.Windows.Forms.ComboBox();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.textBoxDocumentNumber = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.textBoxCourtChequeAmount = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textBoxCourtFeeRefundAmount = new System.Windows.Forms.TextBox();
            this.checkBoxUnnameCheckBox = new System.Windows.Forms.CheckBox();
            this.textBoxCourtFee = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.comboBoxUnnameDropDown1 = new System.Windows.Forms.ComboBox();
            this.comboBoxUnnameDropDown2 = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.textBoxAdditionDetail = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBoxJudgementResult = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.textBoxDebtDetail = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DebtGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegistrationNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContractNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContractDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreditAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalDebtAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BSY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Borrower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Manager = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Guarantor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Condition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBoxLitigateDetail = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.textBoxOperationRegister = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lawyer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.label106 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label107 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBoxCourtLocation = new System.Windows.Forms.TextBox();
            this.textBoxCapital = new System.Windows.Forms.TextBox();
            this.textBoxInterest = new System.Windows.Forms.TextBox();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.comboBoxProsecutorType = new System.Windows.Forms.ComboBox();
            this.comboBoxSueStatus = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.comboBoxExecution = new System.Windows.Forms.ComboBox();
            this.comboBoxFileStatus = new System.Windows.Forms.ComboBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.textBoxPrescriptionPrecludedDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxDebtRecordDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxCloseDate = new System.Windows.Forms.DateTimePicker();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.textBoxSearchFileNo2 = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.textBoxSearchFileNo1 = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dataGridViewCase = new System.Windows.Forms.DataGridView();
            this.View = new System.Windows.Forms.DataGridViewButtonColumn();
            this.AdmissionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileNo1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileNo2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBoxCaseType = new System.Windows.Forms.ComboBox();
            this.textBoxFileNo2 = new System.Windows.Forms.TextBox();
            this.textBoxFileNo1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCase)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "แฟ้มเสร็จ";
            // 
            // textBoxCompletedFileNo
            // 
            this.textBoxCompletedFileNo.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.textBoxCompletedFileNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCompletedFileNo.Location = new System.Drawing.Point(108, 64);
            this.textBoxCompletedFileNo.Name = "textBoxCompletedFileNo";
            this.textBoxCompletedFileNo.Size = new System.Drawing.Size(96, 22);
            this.textBoxCompletedFileNo.TabIndex = 4;
            this.textBoxCompletedFileNo.Text = "X";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "เลขบังคับคดี";
            // 
            // textBoxCaseNo2
            // 
            this.textBoxCaseNo2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.textBoxCaseNo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCaseNo2.Location = new System.Drawing.Point(108, 88);
            this.textBoxCaseNo2.Name = "textBoxCaseNo2";
            this.textBoxCaseNo2.Size = new System.Drawing.Size(96, 22);
            this.textBoxCaseNo2.TabIndex = 7;
            this.textBoxCaseNo2.Text = "X";
            // 
            // textBoxCaseNo1
            // 
            this.textBoxCaseNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCaseNo1.Location = new System.Drawing.Point(84, 88);
            this.textBoxCaseNo1.Name = "textBoxCaseNo1";
            this.textBoxCaseNo1.Size = new System.Drawing.Size(24, 22);
            this.textBoxCaseNo1.TabIndex = 6;
            this.textBoxCaseNo1.Text = "X";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(252, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "เลขบัญชี";
            // 
            // textBoxAccountNo
            // 
            this.textBoxAccountNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxAccountNo.Location = new System.Drawing.Point(304, 40);
            this.textBoxAccountNo.Name = "textBoxAccountNo";
            this.textBoxAccountNo.Size = new System.Drawing.Size(96, 22);
            this.textBoxAccountNo.TabIndex = 9;
            this.textBoxAccountNo.Text = "X";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(216, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "คดีหมายเลขดำที่";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(208, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "คดีหมายเลขแดงที่";
            // 
            // textBoxBlackCaseNo
            // 
            this.textBoxBlackCaseNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxBlackCaseNo.Location = new System.Drawing.Point(304, 64);
            this.textBoxBlackCaseNo.Name = "textBoxBlackCaseNo";
            this.textBoxBlackCaseNo.Size = new System.Drawing.Size(96, 22);
            this.textBoxBlackCaseNo.TabIndex = 12;
            this.textBoxBlackCaseNo.Text = "X";
            // 
            // textBoxRedCaseNo
            // 
            this.textBoxRedCaseNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxRedCaseNo.Location = new System.Drawing.Point(304, 88);
            this.textBoxRedCaseNo.Name = "textBoxRedCaseNo";
            this.textBoxRedCaseNo.Size = new System.Drawing.Size(96, 22);
            this.textBoxRedCaseNo.TabIndex = 13;
            this.textBoxRedCaseNo.Text = "X";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(52, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "ศาล";
            // 
            // comboBoxCourtName
            // 
            this.comboBoxCourtName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxCourtName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxCourtName.FormattingEnabled = true;
            this.comboBoxCourtName.Location = new System.Drawing.Point(80, 112);
            this.comboBoxCourtName.Name = "comboBoxCourtName";
            this.comboBoxCourtName.Size = new System.Drawing.Size(320, 21);
            this.comboBoxCourtName.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(44, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 16);
            this.label8.TabIndex = 16;
            this.label8.Text = "ความ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(136, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 16);
            this.label9.TabIndex = 17;
            this.label9.Text = "ข้อหา";
            // 
            // comboBoxAllegationType
            // 
            this.comboBoxAllegationType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxAllegationType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxAllegationType.FormattingEnabled = true;
            this.comboBoxAllegationType.Location = new System.Drawing.Point(172, 136);
            this.comboBoxAllegationType.Name = "comboBoxAllegationType";
            this.comboBoxAllegationType.Size = new System.Drawing.Size(228, 21);
            this.comboBoxAllegationType.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(44, 164);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 16);
            this.label10.TabIndex = 20;
            this.label10.Text = "โจทย์";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(44, 188);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "ผู้ร้อง";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(36, 212);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 44);
            this.label12.TabIndex = 22;
            this.label12.Text = "จำเลย";
            // 
            // comboBoxProsecutor
            // 
            this.comboBoxProsecutor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxProsecutor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxProsecutor.FormattingEnabled = true;
            this.comboBoxProsecutor.Location = new System.Drawing.Point(80, 160);
            this.comboBoxProsecutor.Name = "comboBoxProsecutor";
            this.comboBoxProsecutor.Size = new System.Drawing.Size(320, 21);
            this.comboBoxProsecutor.TabIndex = 23;
            // 
            // comboBoxComplainant
            // 
            this.comboBoxComplainant.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxComplainant.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxComplainant.FormattingEnabled = true;
            this.comboBoxComplainant.Location = new System.Drawing.Point(80, 184);
            this.comboBoxComplainant.Name = "comboBoxComplainant";
            this.comboBoxComplainant.Size = new System.Drawing.Size(320, 21);
            this.comboBoxComplainant.TabIndex = 24;
            // 
            // comboBoxDefendant
            // 
            this.comboBoxDefendant.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxDefendant.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxDefendant.FormattingEnabled = true;
            this.comboBoxDefendant.Location = new System.Drawing.Point(80, 208);
            this.comboBoxDefendant.Name = "comboBoxDefendant";
            this.comboBoxDefendant.Size = new System.Drawing.Size(320, 21);
            this.comboBoxDefendant.TabIndex = 25;
            // 
            // textBoxDefendantRemark1
            // 
            this.textBoxDefendantRemark1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDefendantRemark1.Location = new System.Drawing.Point(80, 232);
            this.textBoxDefendantRemark1.Name = "textBoxDefendantRemark1";
            this.textBoxDefendantRemark1.Size = new System.Drawing.Size(104, 22);
            this.textBoxDefendantRemark1.TabIndex = 26;
            this.textBoxDefendantRemark1.Text = "X";
            this.textBoxDefendantRemark1.TextChanged += new System.EventHandler(this.textBox10_TextChanged);
            // 
            // textBoxDefendantRemark2
            // 
            this.textBoxDefendantRemark2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDefendantRemark2.Location = new System.Drawing.Point(184, 232);
            this.textBoxDefendantRemark2.Name = "textBoxDefendantRemark2";
            this.textBoxDefendantRemark2.Size = new System.Drawing.Size(104, 22);
            this.textBoxDefendantRemark2.TabIndex = 27;
            this.textBoxDefendantRemark2.Text = "X";
            // 
            // textBoxDefendantRemark3
            // 
            this.textBoxDefendantRemark3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDefendantRemark3.Location = new System.Drawing.Point(292, 232);
            this.textBoxDefendantRemark3.Name = "textBoxDefendantRemark3";
            this.textBoxDefendantRemark3.Size = new System.Drawing.Size(108, 22);
            this.textBoxDefendantRemark3.TabIndex = 28;
            this.textBoxDefendantRemark3.Text = "X";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(8, 260);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1216, 447);
            this.tabControl1.TabIndex = 29;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.comboBoxCaseOfficer);
            this.tabPage1.Controls.Add(this.comboBoxCaseGroup);
            this.tabPage1.Controls.Add(this.comboBoxBSY);
            this.tabPage1.Controls.Add(this.comboBoxMortgage);
            this.tabPage1.Controls.Add(this.comboBoxAssetDetectiveOfficer);
            this.tabPage1.Controls.Add(this.comboBoxWitness);
            this.tabPage1.Controls.Add(this.textBoxCaseWorkRel);
            this.tabPage1.Controls.Add(this.textBoxOperator);
            this.tabPage1.Controls.Add(this.textBoxFollowingResult);
            this.tabPage1.Controls.Add(this.textBoxCallResult);
            this.tabPage1.Controls.Add(this.textBoxCaseResult);
            this.tabPage1.Controls.Add(this.textBoxAccountingOfficer);
            this.tabPage1.Controls.Add(this.textBoxComplaintWorkRel);
            this.tabPage1.Controls.Add(this.textBoxYellowResult);
            this.tabPage1.Controls.Add(this.textBoxRemark);
            this.tabPage1.Controls.Add(this.textBoxDefendantNameList);
            this.tabPage1.Controls.Add(this.textBoxHelperLawyer);
            this.tabPage1.Controls.Add(this.textBoxDisburse3);
            this.tabPage1.Controls.Add(this.textBoxDisburse2);
            this.tabPage1.Controls.Add(this.comboBoxAfterSentenceLawyer);
            this.tabPage1.Controls.Add(this.comboBoxDetectiveLaywer);
            this.tabPage1.Controls.Add(this.label104);
            this.tabPage1.Controls.Add(this.label103);
            this.tabPage1.Controls.Add(this.label102);
            this.tabPage1.Controls.Add(this.label101);
            this.tabPage1.Controls.Add(this.label100);
            this.tabPage1.Controls.Add(this.label99);
            this.tabPage1.Controls.Add(this.label98);
            this.tabPage1.Controls.Add(this.label97);
            this.tabPage1.Controls.Add(this.label96);
            this.tabPage1.Controls.Add(this.label95);
            this.tabPage1.Controls.Add(this.label94);
            this.tabPage1.Controls.Add(this.label93);
            this.tabPage1.Controls.Add(this.textBoxDisburse1);
            this.tabPage1.Controls.Add(this.label92);
            this.tabPage1.Controls.Add(this.label91);
            this.tabPage1.Controls.Add(this.label90);
            this.tabPage1.Controls.Add(this.label89);
            this.tabPage1.Controls.Add(this.label88);
            this.tabPage1.Controls.Add(this.label87);
            this.tabPage1.Controls.Add(this.label86);
            this.tabPage1.Controls.Add(this.label85);
            this.tabPage1.Controls.Add(this.comboBoxComplainantLawyer);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1208, 421);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "รายละเอียดทั่วไป";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // comboBoxCaseOfficer
            // 
            this.comboBoxCaseOfficer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxCaseOfficer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxCaseOfficer.FormattingEnabled = true;
            this.comboBoxCaseOfficer.Items.AddRange(new object[] {
            "เอกรัฐ  สิงห์ดง",
            "จีรวุธ  วงศ์จิรรัตน์ ",
            "ชนะชัย แย้มโกสุมภ์ ",
            "ณัฐพล  โชติวัฒน์วงศ์",
            "ถวิล  ยุบลพริ้ง",
            "ธนกร โสภารินทร์",
            "ภาคภูมิ พ่วงโต ",
            "สราวุฒิ แช่มช้อย"});
            this.comboBoxCaseOfficer.Location = new System.Drawing.Point(931, 85);
            this.comboBoxCaseOfficer.Name = "comboBoxCaseOfficer";
            this.comboBoxCaseOfficer.Size = new System.Drawing.Size(270, 21);
            this.comboBoxCaseOfficer.Sorted = true;
            this.comboBoxCaseOfficer.TabIndex = 107;
            // 
            // comboBoxCaseGroup
            // 
            this.comboBoxCaseGroup.FormattingEnabled = true;
            this.comboBoxCaseGroup.Location = new System.Drawing.Point(931, 34);
            this.comboBoxCaseGroup.Name = "comboBoxCaseGroup";
            this.comboBoxCaseGroup.Size = new System.Drawing.Size(124, 21);
            this.comboBoxCaseGroup.TabIndex = 106;
            this.comboBoxCaseGroup.Text = "X";
            // 
            // comboBoxBSY
            // 
            this.comboBoxBSY.FormattingEnabled = true;
            this.comboBoxBSY.Location = new System.Drawing.Point(699, 110);
            this.comboBoxBSY.Name = "comboBoxBSY";
            this.comboBoxBSY.Size = new System.Drawing.Size(124, 21);
            this.comboBoxBSY.TabIndex = 105;
            this.comboBoxBSY.Text = "X";
            // 
            // comboBoxMortgage
            // 
            this.comboBoxMortgage.FormattingEnabled = true;
            this.comboBoxMortgage.Location = new System.Drawing.Point(699, 85);
            this.comboBoxMortgage.Name = "comboBoxMortgage";
            this.comboBoxMortgage.Size = new System.Drawing.Size(124, 21);
            this.comboBoxMortgage.TabIndex = 104;
            this.comboBoxMortgage.Text = "X";
            // 
            // comboBoxAssetDetectiveOfficer
            // 
            this.comboBoxAssetDetectiveOfficer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxAssetDetectiveOfficer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxAssetDetectiveOfficer.FormattingEnabled = true;
            this.comboBoxAssetDetectiveOfficer.Items.AddRange(new object[] {
            "เอกรัฐ  สิงห์ดง",
            "จีรวุธ  วงศ์จิรรัตน์ ",
            "ชนะชัย แย้มโกสุมภ์ ",
            "ณัฐพล  โชติวัฒน์วงศ์",
            "ถวิล  ยุบลพริ้ง",
            "ธนกร โสภารินทร์",
            "ภาคภูมิ พ่วงโต ",
            "สราวุฒิ แช่มช้อย"});
            this.comboBoxAssetDetectiveOfficer.Location = new System.Drawing.Point(931, 59);
            this.comboBoxAssetDetectiveOfficer.Name = "comboBoxAssetDetectiveOfficer";
            this.comboBoxAssetDetectiveOfficer.Size = new System.Drawing.Size(270, 21);
            this.comboBoxAssetDetectiveOfficer.Sorted = true;
            this.comboBoxAssetDetectiveOfficer.TabIndex = 104;
            // 
            // comboBoxWitness
            // 
            this.comboBoxWitness.FormattingEnabled = true;
            this.comboBoxWitness.Location = new System.Drawing.Point(699, 34);
            this.comboBoxWitness.Name = "comboBoxWitness";
            this.comboBoxWitness.Size = new System.Drawing.Size(124, 21);
            this.comboBoxWitness.TabIndex = 103;
            this.comboBoxWitness.Text = "X";
            // 
            // textBoxCaseWorkRel
            // 
            this.textBoxCaseWorkRel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCaseWorkRel.Location = new System.Drawing.Point(931, 108);
            this.textBoxCaseWorkRel.Name = "textBoxCaseWorkRel";
            this.textBoxCaseWorkRel.Size = new System.Drawing.Size(124, 22);
            this.textBoxCaseWorkRel.TabIndex = 102;
            this.textBoxCaseWorkRel.Text = "X";
            // 
            // textBoxOperator
            // 
            this.textBoxOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxOperator.Location = new System.Drawing.Point(699, 58);
            this.textBoxOperator.Name = "textBoxOperator";
            this.textBoxOperator.Size = new System.Drawing.Size(96, 22);
            this.textBoxOperator.TabIndex = 101;
            this.textBoxOperator.Text = "X";
            // 
            // textBoxFollowingResult
            // 
            this.textBoxFollowingResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxFollowingResult.Location = new System.Drawing.Point(699, 180);
            this.textBoxFollowingResult.Name = "textBoxFollowingResult";
            this.textBoxFollowingResult.Size = new System.Drawing.Size(96, 22);
            this.textBoxFollowingResult.TabIndex = 100;
            this.textBoxFollowingResult.Text = "X";
            // 
            // textBoxCallResult
            // 
            this.textBoxCallResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCallResult.Location = new System.Drawing.Point(699, 205);
            this.textBoxCallResult.Name = "textBoxCallResult";
            this.textBoxCallResult.Size = new System.Drawing.Size(96, 22);
            this.textBoxCallResult.TabIndex = 99;
            this.textBoxCallResult.Text = "X";
            // 
            // textBoxCaseResult
            // 
            this.textBoxCaseResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCaseResult.Location = new System.Drawing.Point(699, 230);
            this.textBoxCaseResult.Name = "textBoxCaseResult";
            this.textBoxCaseResult.Size = new System.Drawing.Size(96, 22);
            this.textBoxCaseResult.TabIndex = 98;
            this.textBoxCaseResult.Text = "X";
            // 
            // textBoxAccountingOfficer
            // 
            this.textBoxAccountingOfficer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxAccountingOfficer.Location = new System.Drawing.Point(699, 304);
            this.textBoxAccountingOfficer.Name = "textBoxAccountingOfficer";
            this.textBoxAccountingOfficer.Size = new System.Drawing.Size(108, 22);
            this.textBoxAccountingOfficer.TabIndex = 97;
            this.textBoxAccountingOfficer.Text = "X";
            // 
            // textBoxComplaintWorkRel
            // 
            this.textBoxComplaintWorkRel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxComplaintWorkRel.Location = new System.Drawing.Point(105, 354);
            this.textBoxComplaintWorkRel.Name = "textBoxComplaintWorkRel";
            this.textBoxComplaintWorkRel.Size = new System.Drawing.Size(248, 22);
            this.textBoxComplaintWorkRel.TabIndex = 96;
            this.textBoxComplaintWorkRel.Text = "X";
            // 
            // textBoxYellowResult
            // 
            this.textBoxYellowResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxYellowResult.Location = new System.Drawing.Point(105, 328);
            this.textBoxYellowResult.Name = "textBoxYellowResult";
            this.textBoxYellowResult.Size = new System.Drawing.Size(316, 22);
            this.textBoxYellowResult.TabIndex = 95;
            this.textBoxYellowResult.Text = "X";
            // 
            // textBoxRemark
            // 
            this.textBoxRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxRemark.Location = new System.Drawing.Point(105, 256);
            this.textBoxRemark.Multiline = true;
            this.textBoxRemark.Name = "textBoxRemark";
            this.textBoxRemark.Size = new System.Drawing.Size(348, 68);
            this.textBoxRemark.TabIndex = 94;
            this.textBoxRemark.Text = "X";
            // 
            // textBoxDefendantNameList
            // 
            this.textBoxDefendantNameList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDefendantNameList.Location = new System.Drawing.Point(105, 184);
            this.textBoxDefendantNameList.Multiline = true;
            this.textBoxDefendantNameList.Name = "textBoxDefendantNameList";
            this.textBoxDefendantNameList.Size = new System.Drawing.Size(348, 68);
            this.textBoxDefendantNameList.TabIndex = 93;
            this.textBoxDefendantNameList.Text = "X";
            // 
            // textBoxHelperLawyer
            // 
            this.textBoxHelperLawyer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxHelperLawyer.Location = new System.Drawing.Point(105, 112);
            this.textBoxHelperLawyer.Multiline = true;
            this.textBoxHelperLawyer.Name = "textBoxHelperLawyer";
            this.textBoxHelperLawyer.Size = new System.Drawing.Size(348, 68);
            this.textBoxHelperLawyer.TabIndex = 68;
            this.textBoxHelperLawyer.Text = "X";
            // 
            // textBoxDisburse3
            // 
            this.textBoxDisburse3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDisburse3.Location = new System.Drawing.Point(431, 84);
            this.textBoxDisburse3.Name = "textBoxDisburse3";
            this.textBoxDisburse3.Size = new System.Drawing.Size(96, 22);
            this.textBoxDisburse3.TabIndex = 92;
            // 
            // textBoxDisburse2
            // 
            this.textBoxDisburse2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDisburse2.Location = new System.Drawing.Point(431, 58);
            this.textBoxDisburse2.Name = "textBoxDisburse2";
            this.textBoxDisburse2.Size = new System.Drawing.Size(96, 22);
            this.textBoxDisburse2.TabIndex = 91;
            // 
            // comboBoxAfterSentenceLawyer
            // 
            this.comboBoxAfterSentenceLawyer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxAfterSentenceLawyer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxAfterSentenceLawyer.FormattingEnabled = true;
            this.comboBoxAfterSentenceLawyer.Location = new System.Drawing.Point(105, 84);
            this.comboBoxAfterSentenceLawyer.Name = "comboBoxAfterSentenceLawyer";
            this.comboBoxAfterSentenceLawyer.Size = new System.Drawing.Size(320, 21);
            this.comboBoxAfterSentenceLawyer.Sorted = true;
            this.comboBoxAfterSentenceLawyer.TabIndex = 90;
            // 
            // comboBoxDetectiveLaywer
            // 
            this.comboBoxDetectiveLaywer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxDetectiveLaywer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxDetectiveLaywer.FormattingEnabled = true;
            this.comboBoxDetectiveLaywer.Location = new System.Drawing.Point(105, 60);
            this.comboBoxDetectiveLaywer.Name = "comboBoxDetectiveLaywer";
            this.comboBoxDetectiveLaywer.Size = new System.Drawing.Size(320, 21);
            this.comboBoxDetectiveLaywer.Sorted = true;
            this.comboBoxDetectiveLaywer.TabIndex = 89;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(604, 308);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(92, 16);
            this.label104.TabIndex = 88;
            this.label104.Text = "เจ้าหน้าที่ดูแลบัญชี";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(610, 233);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(86, 16);
            this.label103.TabIndex = 87;
            this.label103.Text = "ผลหมายคำบังคับ";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(628, 208);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(68, 16);
            this.label102.TabIndex = 86;
            this.label102.Text = "ผลหมายเรียก";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(625, 183);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(71, 16);
            this.label101.TabIndex = 85;
            this.label101.Text = "ผลการติดตาม";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(825, 112);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(102, 16);
            this.label100.TabIndex = 84;
            this.label100.Text = "งานบังคับคดี (Ref.)";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(835, 87);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(88, 16);
            this.label99.TabIndex = 83;
            this.label99.Text = "พนักงานบังคับคดี";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(832, 61);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(89, 16);
            this.label98.TabIndex = 82;
            this.label98.Text = "พนักงานสืบทรัพย์";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(835, 36);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(86, 16);
            this.label97.TabIndex = 81;
            this.label97.Text = "กลุ่มงานบังคับคดี";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(659, 110);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(37, 16);
            this.label96.TabIndex = 80;
            this.label96.Text = "บ.ส.ย.";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(654, 87);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(38, 16);
            this.label95.TabIndex = 79;
            this.label95.Text = "จำนอง";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(633, 61);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(61, 16);
            this.label94.TabIndex = 78;
            this.label94.Text = "ผู้ปฏิบัติงาน";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(659, 36);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(34, 16);
            this.label93.TabIndex = 77;
            this.label93.Text = "พยาน";
            // 
            // textBoxDisburse1
            // 
            this.textBoxDisburse1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDisburse1.Location = new System.Drawing.Point(431, 33);
            this.textBoxDisburse1.Name = "textBoxDisburse1";
            this.textBoxDisburse1.Size = new System.Drawing.Size(96, 22);
            this.textBoxDisburse1.TabIndex = 68;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(434, 12);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(41, 16);
            this.label92.TabIndex = 76;
            this.label92.Text = "เบิกจ่าย";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(18, 356);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(81, 16);
            this.label91.TabIndex = 75;
            this.label91.Text = "งานฟ้อง (Ref.)";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(4, 330);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(95, 16);
            this.label90.TabIndex = 74;
            this.label90.Text = "ผลใบเหลืองตอบรับ";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(47, 259);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(52, 16);
            this.label89.TabIndex = 73;
            this.label89.Text = "หมายเหตุ";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(52, 187);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(47, 16);
            this.label88.TabIndex = 72;
            this.label88.Text = "ชื่อจำเลย";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(39, 115);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(60, 16);
            this.label87.TabIndex = 71;
            this.label87.Text = "ทนายผู้ช่วย";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(6, 86);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(93, 16);
            this.label86.TabIndex = 70;
            this.label86.Text = "ทนายหลังพิพากษา";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(20, 62);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(79, 16);
            this.label85.TabIndex = 69;
            this.label85.Text = "ทนายตวามสืบฯ";
            // 
            // comboBoxComplainantLawyer
            // 
            this.comboBoxComplainantLawyer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxComplainantLawyer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxComplainantLawyer.FormattingEnabled = true;
            this.comboBoxComplainantLawyer.Location = new System.Drawing.Point(105, 34);
            this.comboBoxComplainantLawyer.Name = "comboBoxComplainantLawyer";
            this.comboBoxComplainantLawyer.Size = new System.Drawing.Size(320, 21);
            this.comboBoxComplainantLawyer.Sorted = true;
            this.comboBoxComplainantLawyer.TabIndex = 68;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(45, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 16);
            this.label13.TabIndex = 68;
            this.label13.Text = "ทนายฟ้อง";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dateTimePickerPaymentWithdrawalDate3);
            this.tabPage2.Controls.Add(this.dateTimePickerPaymentWithdrawalDate2);
            this.tabPage2.Controls.Add(this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate);
            this.tabPage2.Controls.Add(this.dateTimePickerPaymentWithdrawalDate1);
            this.tabPage2.Controls.Add(this.dateTimePickerWithdrawalDividedAttachedFundsDate);
            this.tabPage2.Controls.Add(this.dateTimePickerPermittedDate2);
            this.tabPage2.Controls.Add(this.dateTimePickerPermittedDate1);
            this.tabPage2.Controls.Add(this.dateTimePickerWithdrawalDate1);
            this.tabPage2.Controls.Add(this.dateTimePickerSubrogatedWithdrawalDate);
            this.tabPage2.Controls.Add(this.dateTimePickerWithdrawalDate2);
            this.tabPage2.Controls.Add(this.dateTimePickerWithdrawalAttachment2);
            this.tabPage2.Controls.Add(this.dateTimePickerExecutationAttachment2);
            this.tabPage2.Controls.Add(this.dateTimePickerWithdrawalAttachment1);
            this.tabPage2.Controls.Add(this.dateTimePickerExecutationAttachment1);
            this.tabPage2.Controls.Add(this.dateTimePickerEndOfSellingAuctionDueDate);
            this.tabPage2.Controls.Add(this.dateTimePickerExpiredCaseDate);
            this.tabPage2.Controls.Add(this.dateTimePickerAcquiringProxyDate);
            this.tabPage2.Controls.Add(this.dateTimePickerSendingProxyDate);
            this.tabPage2.Controls.Add(this.dateTimePickerAbsoluteReceivershipDate);
            this.tabPage2.Controls.Add(this.dateTimePickerAttachmentAfterSellingDate);
            this.tabPage2.Controls.Add(this.dateTimePickerRequestForPaymentDate);
            this.tabPage2.Controls.Add(this.dateTimePickerDivisionDate);
            this.tabPage2.Controls.Add(this.dateTimePickerSubrogationDate);
            this.tabPage2.Controls.Add(this.dateTimePickerApplyingDivisionDate);
            this.tabPage2.Controls.Add(this.dateTimePickerSeizingPropertiesDate);
            this.tabPage2.Controls.Add(this.dateTimePickerApplyingAttachmentDate);
            this.tabPage2.Controls.Add(this.dateTimePickerAcquiringSuccessorDate);
            this.tabPage2.Controls.Add(this.dateTimePickerOpeningCaseDate);
            this.tabPage2.Controls.Add(this.dateTimePickerReturningChequeDate);
            this.tabPage2.Controls.Add(this.dateTimePickerCourtChequeDate);
            this.tabPage2.Controls.Add(this.dateTimePickerAppealJudgementDate);
            this.tabPage2.Controls.Add(this.dateTimePickerAcceptingAppealJudgementDate);
            this.tabPage2.Controls.Add(this.dateTimePickerTerminatingCaseApprovalDate);
            this.tabPage2.Controls.Add(this.dateTimePickerWithdrawalDate);
            this.tabPage2.Controls.Add(this.dateTimePickerJudgementExecutationClosingDate);
            this.tabPage2.Controls.Add(this.dateTimePickerDueDate);
            this.tabPage2.Controls.Add(this.dateTimePickerClaimDate);
            this.tabPage2.Controls.Add(this.dateTimePickerReturningFileDate);
            this.tabPage2.Controls.Add(this.dateTimePickerCourtFeeRefundRequestDate);
            this.tabPage2.Controls.Add(this.dateTimePickerLodgingAppealDate);
            this.tabPage2.Controls.Add(this.dateTimePickerApplyingPetitionDate);
            this.tabPage2.Controls.Add(this.dateTimePickJudgementExecutationDate);
            this.tabPage2.Controls.Add(this.dateTimePickerIssuingWarrantDate);
            this.tabPage2.Controls.Add(this.dateTimePickerMandatoryDate);
            this.tabPage2.Controls.Add(this.dateTimePickerDayOfJudgment);
            this.tabPage2.Controls.Add(this.dateTimePickerFilingDate);
            this.tabPage2.Controls.Add(this.dateTimePickerNoticeDate);
            this.tabPage2.Controls.Add(this.dateTimePickerAdmissionDate);
            this.tabPage2.Controls.Add(this.label84);
            this.tabPage2.Controls.Add(this.label83);
            this.tabPage2.Controls.Add(this.comboBoxUnnameDropDown3);
            this.tabPage2.Controls.Add(this.label82);
            this.tabPage2.Controls.Add(this.label81);
            this.tabPage2.Controls.Add(this.label80);
            this.tabPage2.Controls.Add(this.label79);
            this.tabPage2.Controls.Add(this.label78);
            this.tabPage2.Controls.Add(this.label77);
            this.tabPage2.Controls.Add(this.label76);
            this.tabPage2.Controls.Add(this.label75);
            this.tabPage2.Controls.Add(this.label74);
            this.tabPage2.Controls.Add(this.label73);
            this.tabPage2.Controls.Add(this.label72);
            this.tabPage2.Controls.Add(this.label71);
            this.tabPage2.Controls.Add(this.textBoxDocumentNumber);
            this.tabPage2.Controls.Add(this.label70);
            this.tabPage2.Controls.Add(this.label69);
            this.tabPage2.Controls.Add(this.label68);
            this.tabPage2.Controls.Add(this.label67);
            this.tabPage2.Controls.Add(this.label66);
            this.tabPage2.Controls.Add(this.label65);
            this.tabPage2.Controls.Add(this.label64);
            this.tabPage2.Controls.Add(this.label63);
            this.tabPage2.Controls.Add(this.label62);
            this.tabPage2.Controls.Add(this.label61);
            this.tabPage2.Controls.Add(this.label60);
            this.tabPage2.Controls.Add(this.label59);
            this.tabPage2.Controls.Add(this.label58);
            this.tabPage2.Controls.Add(this.label56);
            this.tabPage2.Controls.Add(this.label57);
            this.tabPage2.Controls.Add(this.label55);
            this.tabPage2.Controls.Add(this.textBoxCourtChequeAmount);
            this.tabPage2.Controls.Add(this.label54);
            this.tabPage2.Controls.Add(this.textBoxCourtFeeRefundAmount);
            this.tabPage2.Controls.Add(this.checkBoxUnnameCheckBox);
            this.tabPage2.Controls.Add(this.textBoxCourtFee);
            this.tabPage2.Controls.Add(this.label53);
            this.tabPage2.Controls.Add(this.label52);
            this.tabPage2.Controls.Add(this.label51);
            this.tabPage2.Controls.Add(this.label50);
            this.tabPage2.Controls.Add(this.label49);
            this.tabPage2.Controls.Add(this.label48);
            this.tabPage2.Controls.Add(this.label47);
            this.tabPage2.Controls.Add(this.comboBoxUnnameDropDown1);
            this.tabPage2.Controls.Add(this.comboBoxUnnameDropDown2);
            this.tabPage2.Controls.Add(this.label46);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.label45);
            this.tabPage2.Controls.Add(this.textBoxAdditionDetail);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.label38);
            this.tabPage2.Controls.Add(this.label37);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1208, 421);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "รายละเอียดวันที่";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dateTimePickerPaymentWithdrawalDate3
            // 
            this.dateTimePickerPaymentWithdrawalDate3.CustomFormat = " ";
            this.dateTimePickerPaymentWithdrawalDate3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPaymentWithdrawalDate3.Location = new System.Drawing.Point(1104, 275);
            this.dateTimePickerPaymentWithdrawalDate3.Name = "dateTimePickerPaymentWithdrawalDate3";
            this.dateTimePickerPaymentWithdrawalDate3.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerPaymentWithdrawalDate3.TabIndex = 219;
            this.dateTimePickerPaymentWithdrawalDate3.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerPaymentWithdrawalDate3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerPaymentWithdrawalDate2
            // 
            this.dateTimePickerPaymentWithdrawalDate2.CustomFormat = " ";
            this.dateTimePickerPaymentWithdrawalDate2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPaymentWithdrawalDate2.Location = new System.Drawing.Point(968, 275);
            this.dateTimePickerPaymentWithdrawalDate2.Name = "dateTimePickerPaymentWithdrawalDate2";
            this.dateTimePickerPaymentWithdrawalDate2.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerPaymentWithdrawalDate2.TabIndex = 218;
            this.dateTimePickerPaymentWithdrawalDate2.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerPaymentWithdrawalDate2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate
            // 
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate.CustomFormat = " ";
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate.Location = new System.Drawing.Point(840, 298);
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate.Name = "dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate";
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate.TabIndex = 217;
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerPaymentWithdrawalDate1
            // 
            this.dateTimePickerPaymentWithdrawalDate1.CustomFormat = " ";
            this.dateTimePickerPaymentWithdrawalDate1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPaymentWithdrawalDate1.Location = new System.Drawing.Point(840, 275);
            this.dateTimePickerPaymentWithdrawalDate1.Name = "dateTimePickerPaymentWithdrawalDate1";
            this.dateTimePickerPaymentWithdrawalDate1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerPaymentWithdrawalDate1.TabIndex = 216;
            this.dateTimePickerPaymentWithdrawalDate1.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerPaymentWithdrawalDate1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerWithdrawalDividedAttachedFundsDate
            // 
            this.dateTimePickerWithdrawalDividedAttachedFundsDate.CustomFormat = " ";
            this.dateTimePickerWithdrawalDividedAttachedFundsDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerWithdrawalDividedAttachedFundsDate.Location = new System.Drawing.Point(840, 251);
            this.dateTimePickerWithdrawalDividedAttachedFundsDate.Name = "dateTimePickerWithdrawalDividedAttachedFundsDate";
            this.dateTimePickerWithdrawalDividedAttachedFundsDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerWithdrawalDividedAttachedFundsDate.TabIndex = 215;
            this.dateTimePickerWithdrawalDividedAttachedFundsDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerWithdrawalDividedAttachedFundsDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerPermittedDate2
            // 
            this.dateTimePickerPermittedDate2.CustomFormat = " ";
            this.dateTimePickerPermittedDate2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPermittedDate2.Location = new System.Drawing.Point(787, 179);
            this.dateTimePickerPermittedDate2.Name = "dateTimePickerPermittedDate2";
            this.dateTimePickerPermittedDate2.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerPermittedDate2.TabIndex = 214;
            this.dateTimePickerPermittedDate2.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerPermittedDate2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerPermittedDate1
            // 
            this.dateTimePickerPermittedDate1.CustomFormat = " ";
            this.dateTimePickerPermittedDate1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerPermittedDate1.Location = new System.Drawing.Point(787, 156);
            this.dateTimePickerPermittedDate1.Name = "dateTimePickerPermittedDate1";
            this.dateTimePickerPermittedDate1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerPermittedDate1.TabIndex = 213;
            this.dateTimePickerPermittedDate1.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerPermittedDate1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerWithdrawalDate1
            // 
            this.dateTimePickerWithdrawalDate1.CustomFormat = " ";
            this.dateTimePickerWithdrawalDate1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerWithdrawalDate1.Location = new System.Drawing.Point(943, 156);
            this.dateTimePickerWithdrawalDate1.Name = "dateTimePickerWithdrawalDate1";
            this.dateTimePickerWithdrawalDate1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerWithdrawalDate1.TabIndex = 212;
            this.dateTimePickerWithdrawalDate1.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerWithdrawalDate1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerSubrogatedWithdrawalDate
            // 
            this.dateTimePickerSubrogatedWithdrawalDate.CustomFormat = " ";
            this.dateTimePickerSubrogatedWithdrawalDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerSubrogatedWithdrawalDate.Location = new System.Drawing.Point(1104, 179);
            this.dateTimePickerSubrogatedWithdrawalDate.Name = "dateTimePickerSubrogatedWithdrawalDate";
            this.dateTimePickerSubrogatedWithdrawalDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerSubrogatedWithdrawalDate.TabIndex = 211;
            this.dateTimePickerSubrogatedWithdrawalDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerSubrogatedWithdrawalDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerWithdrawalDate2
            // 
            this.dateTimePickerWithdrawalDate2.CustomFormat = " ";
            this.dateTimePickerWithdrawalDate2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerWithdrawalDate2.Location = new System.Drawing.Point(1104, 156);
            this.dateTimePickerWithdrawalDate2.Name = "dateTimePickerWithdrawalDate2";
            this.dateTimePickerWithdrawalDate2.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerWithdrawalDate2.TabIndex = 210;
            this.dateTimePickerWithdrawalDate2.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerWithdrawalDate2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerWithdrawalAttachment2
            // 
            this.dateTimePickerWithdrawalAttachment2.CustomFormat = " ";
            this.dateTimePickerWithdrawalAttachment2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerWithdrawalAttachment2.Location = new System.Drawing.Point(1104, 132);
            this.dateTimePickerWithdrawalAttachment2.Name = "dateTimePickerWithdrawalAttachment2";
            this.dateTimePickerWithdrawalAttachment2.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerWithdrawalAttachment2.TabIndex = 209;
            this.dateTimePickerWithdrawalAttachment2.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerWithdrawalAttachment2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerExecutationAttachment2
            // 
            this.dateTimePickerExecutationAttachment2.CustomFormat = " ";
            this.dateTimePickerExecutationAttachment2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerExecutationAttachment2.Location = new System.Drawing.Point(1104, 108);
            this.dateTimePickerExecutationAttachment2.Name = "dateTimePickerExecutationAttachment2";
            this.dateTimePickerExecutationAttachment2.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerExecutationAttachment2.TabIndex = 208;
            this.dateTimePickerExecutationAttachment2.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerExecutationAttachment2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerWithdrawalAttachment1
            // 
            this.dateTimePickerWithdrawalAttachment1.CustomFormat = " ";
            this.dateTimePickerWithdrawalAttachment1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerWithdrawalAttachment1.Location = new System.Drawing.Point(840, 132);
            this.dateTimePickerWithdrawalAttachment1.Name = "dateTimePickerWithdrawalAttachment1";
            this.dateTimePickerWithdrawalAttachment1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerWithdrawalAttachment1.TabIndex = 207;
            this.dateTimePickerWithdrawalAttachment1.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerWithdrawalAttachment1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerExecutationAttachment1
            // 
            this.dateTimePickerExecutationAttachment1.CustomFormat = " ";
            this.dateTimePickerExecutationAttachment1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerExecutationAttachment1.Location = new System.Drawing.Point(840, 108);
            this.dateTimePickerExecutationAttachment1.Name = "dateTimePickerExecutationAttachment1";
            this.dateTimePickerExecutationAttachment1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerExecutationAttachment1.TabIndex = 206;
            this.dateTimePickerExecutationAttachment1.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerExecutationAttachment1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerEndOfSellingAuctionDueDate
            // 
            this.dateTimePickerEndOfSellingAuctionDueDate.CustomFormat = " ";
            this.dateTimePickerEndOfSellingAuctionDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerEndOfSellingAuctionDueDate.Location = new System.Drawing.Point(1104, 60);
            this.dateTimePickerEndOfSellingAuctionDueDate.Name = "dateTimePickerEndOfSellingAuctionDueDate";
            this.dateTimePickerEndOfSellingAuctionDueDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerEndOfSellingAuctionDueDate.TabIndex = 205;
            this.dateTimePickerEndOfSellingAuctionDueDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerEndOfSellingAuctionDueDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerExpiredCaseDate
            // 
            this.dateTimePickerExpiredCaseDate.CustomFormat = " ";
            this.dateTimePickerExpiredCaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerExpiredCaseDate.Location = new System.Drawing.Point(1104, 37);
            this.dateTimePickerExpiredCaseDate.Name = "dateTimePickerExpiredCaseDate";
            this.dateTimePickerExpiredCaseDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerExpiredCaseDate.TabIndex = 204;
            this.dateTimePickerExpiredCaseDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerExpiredCaseDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerAcquiringProxyDate
            // 
            this.dateTimePickerAcquiringProxyDate.CustomFormat = " ";
            this.dateTimePickerAcquiringProxyDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerAcquiringProxyDate.Location = new System.Drawing.Point(1104, 14);
            this.dateTimePickerAcquiringProxyDate.Name = "dateTimePickerAcquiringProxyDate";
            this.dateTimePickerAcquiringProxyDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerAcquiringProxyDate.TabIndex = 203;
            this.dateTimePickerAcquiringProxyDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerAcquiringProxyDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerSendingProxyDate
            // 
            this.dateTimePickerSendingProxyDate.CustomFormat = " ";
            this.dateTimePickerSendingProxyDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerSendingProxyDate.Location = new System.Drawing.Point(840, 14);
            this.dateTimePickerSendingProxyDate.Name = "dateTimePickerSendingProxyDate";
            this.dateTimePickerSendingProxyDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerSendingProxyDate.TabIndex = 202;
            this.dateTimePickerSendingProxyDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerSendingProxyDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerAbsoluteReceivershipDate
            // 
            this.dateTimePickerAbsoluteReceivershipDate.CustomFormat = " ";
            this.dateTimePickerAbsoluteReceivershipDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerAbsoluteReceivershipDate.Location = new System.Drawing.Point(614, 323);
            this.dateTimePickerAbsoluteReceivershipDate.Name = "dateTimePickerAbsoluteReceivershipDate";
            this.dateTimePickerAbsoluteReceivershipDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerAbsoluteReceivershipDate.TabIndex = 201;
            this.dateTimePickerAbsoluteReceivershipDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerAbsoluteReceivershipDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerAttachmentAfterSellingDate
            // 
            this.dateTimePickerAttachmentAfterSellingDate.CustomFormat = " ";
            this.dateTimePickerAttachmentAfterSellingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerAttachmentAfterSellingDate.Location = new System.Drawing.Point(614, 298);
            this.dateTimePickerAttachmentAfterSellingDate.Name = "dateTimePickerAttachmentAfterSellingDate";
            this.dateTimePickerAttachmentAfterSellingDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerAttachmentAfterSellingDate.TabIndex = 200;
            this.dateTimePickerAttachmentAfterSellingDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerAttachmentAfterSellingDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerRequestForPaymentDate
            // 
            this.dateTimePickerRequestForPaymentDate.CustomFormat = " ";
            this.dateTimePickerRequestForPaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerRequestForPaymentDate.Location = new System.Drawing.Point(614, 275);
            this.dateTimePickerRequestForPaymentDate.Name = "dateTimePickerRequestForPaymentDate";
            this.dateTimePickerRequestForPaymentDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerRequestForPaymentDate.TabIndex = 199;
            this.dateTimePickerRequestForPaymentDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerRequestForPaymentDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerDivisionDate
            // 
            this.dateTimePickerDivisionDate.CustomFormat = " ";
            this.dateTimePickerDivisionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDivisionDate.Location = new System.Drawing.Point(614, 251);
            this.dateTimePickerDivisionDate.Name = "dateTimePickerDivisionDate";
            this.dateTimePickerDivisionDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerDivisionDate.TabIndex = 198;
            this.dateTimePickerDivisionDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerDivisionDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerSubrogationDate
            // 
            this.dateTimePickerSubrogationDate.CustomFormat = " ";
            this.dateTimePickerSubrogationDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerSubrogationDate.Location = new System.Drawing.Point(614, 179);
            this.dateTimePickerSubrogationDate.Name = "dateTimePickerSubrogationDate";
            this.dateTimePickerSubrogationDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerSubrogationDate.TabIndex = 197;
            this.dateTimePickerSubrogationDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerSubrogationDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerApplyingDivisionDate
            // 
            this.dateTimePickerApplyingDivisionDate.CustomFormat = " ";
            this.dateTimePickerApplyingDivisionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerApplyingDivisionDate.Location = new System.Drawing.Point(614, 156);
            this.dateTimePickerApplyingDivisionDate.Name = "dateTimePickerApplyingDivisionDate";
            this.dateTimePickerApplyingDivisionDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerApplyingDivisionDate.TabIndex = 196;
            this.dateTimePickerApplyingDivisionDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerApplyingDivisionDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerSeizingPropertiesDate
            // 
            this.dateTimePickerSeizingPropertiesDate.CustomFormat = " ";
            this.dateTimePickerSeizingPropertiesDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerSeizingPropertiesDate.Location = new System.Drawing.Point(614, 132);
            this.dateTimePickerSeizingPropertiesDate.Name = "dateTimePickerSeizingPropertiesDate";
            this.dateTimePickerSeizingPropertiesDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerSeizingPropertiesDate.TabIndex = 195;
            this.dateTimePickerSeizingPropertiesDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerSeizingPropertiesDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerApplyingAttachmentDate
            // 
            this.dateTimePickerApplyingAttachmentDate.CustomFormat = " ";
            this.dateTimePickerApplyingAttachmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerApplyingAttachmentDate.Location = new System.Drawing.Point(614, 108);
            this.dateTimePickerApplyingAttachmentDate.Name = "dateTimePickerApplyingAttachmentDate";
            this.dateTimePickerApplyingAttachmentDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerApplyingAttachmentDate.TabIndex = 194;
            this.dateTimePickerApplyingAttachmentDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerApplyingAttachmentDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerAcquiringSuccessorDate
            // 
            this.dateTimePickerAcquiringSuccessorDate.CustomFormat = " ";
            this.dateTimePickerAcquiringSuccessorDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerAcquiringSuccessorDate.Location = new System.Drawing.Point(614, 37);
            this.dateTimePickerAcquiringSuccessorDate.Name = "dateTimePickerAcquiringSuccessorDate";
            this.dateTimePickerAcquiringSuccessorDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerAcquiringSuccessorDate.TabIndex = 193;
            this.dateTimePickerAcquiringSuccessorDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerAcquiringSuccessorDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerOpeningCaseDate
            // 
            this.dateTimePickerOpeningCaseDate.CustomFormat = " ";
            this.dateTimePickerOpeningCaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerOpeningCaseDate.Location = new System.Drawing.Point(614, 14);
            this.dateTimePickerOpeningCaseDate.Name = "dateTimePickerOpeningCaseDate";
            this.dateTimePickerOpeningCaseDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerOpeningCaseDate.TabIndex = 192;
            this.dateTimePickerOpeningCaseDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerOpeningCaseDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerReturningChequeDate
            // 
            this.dateTimePickerReturningChequeDate.CustomFormat = " ";
            this.dateTimePickerReturningChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerReturningChequeDate.Location = new System.Drawing.Point(359, 251);
            this.dateTimePickerReturningChequeDate.Name = "dateTimePickerReturningChequeDate";
            this.dateTimePickerReturningChequeDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerReturningChequeDate.TabIndex = 191;
            this.dateTimePickerReturningChequeDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerReturningChequeDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerCourtChequeDate
            // 
            this.dateTimePickerCourtChequeDate.CustomFormat = " ";
            this.dateTimePickerCourtChequeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerCourtChequeDate.Location = new System.Drawing.Point(359, 227);
            this.dateTimePickerCourtChequeDate.Name = "dateTimePickerCourtChequeDate";
            this.dateTimePickerCourtChequeDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerCourtChequeDate.TabIndex = 190;
            this.dateTimePickerCourtChequeDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerCourtChequeDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerAppealJudgementDate
            // 
            this.dateTimePickerAppealJudgementDate.CustomFormat = " ";
            this.dateTimePickerAppealJudgementDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerAppealJudgementDate.Location = new System.Drawing.Point(359, 203);
            this.dateTimePickerAppealJudgementDate.Name = "dateTimePickerAppealJudgementDate";
            this.dateTimePickerAppealJudgementDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerAppealJudgementDate.TabIndex = 189;
            this.dateTimePickerAppealJudgementDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerAppealJudgementDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerAcceptingAppealJudgementDate
            // 
            this.dateTimePickerAcceptingAppealJudgementDate.CustomFormat = " ";
            this.dateTimePickerAcceptingAppealJudgementDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerAcceptingAppealJudgementDate.Location = new System.Drawing.Point(359, 179);
            this.dateTimePickerAcceptingAppealJudgementDate.Name = "dateTimePickerAcceptingAppealJudgementDate";
            this.dateTimePickerAcceptingAppealJudgementDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerAcceptingAppealJudgementDate.TabIndex = 188;
            this.dateTimePickerAcceptingAppealJudgementDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerAcceptingAppealJudgementDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerTerminatingCaseApprovalDate
            // 
            this.dateTimePickerTerminatingCaseApprovalDate.CustomFormat = " ";
            this.dateTimePickerTerminatingCaseApprovalDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerTerminatingCaseApprovalDate.Location = new System.Drawing.Point(359, 156);
            this.dateTimePickerTerminatingCaseApprovalDate.Name = "dateTimePickerTerminatingCaseApprovalDate";
            this.dateTimePickerTerminatingCaseApprovalDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerTerminatingCaseApprovalDate.TabIndex = 187;
            this.dateTimePickerTerminatingCaseApprovalDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerTerminatingCaseApprovalDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerWithdrawalDate
            // 
            this.dateTimePickerWithdrawalDate.CustomFormat = " ";
            this.dateTimePickerWithdrawalDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerWithdrawalDate.Location = new System.Drawing.Point(359, 132);
            this.dateTimePickerWithdrawalDate.Name = "dateTimePickerWithdrawalDate";
            this.dateTimePickerWithdrawalDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerWithdrawalDate.TabIndex = 186;
            this.dateTimePickerWithdrawalDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerWithdrawalDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerJudgementExecutationClosingDate
            // 
            this.dateTimePickerJudgementExecutationClosingDate.CustomFormat = " ";
            this.dateTimePickerJudgementExecutationClosingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerJudgementExecutationClosingDate.Location = new System.Drawing.Point(359, 108);
            this.dateTimePickerJudgementExecutationClosingDate.Name = "dateTimePickerJudgementExecutationClosingDate";
            this.dateTimePickerJudgementExecutationClosingDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerJudgementExecutationClosingDate.TabIndex = 185;
            this.dateTimePickerJudgementExecutationClosingDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerJudgementExecutationClosingDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerDueDate
            // 
            this.dateTimePickerDueDate.CustomFormat = " ";
            this.dateTimePickerDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDueDate.Location = new System.Drawing.Point(359, 37);
            this.dateTimePickerDueDate.Name = "dateTimePickerDueDate";
            this.dateTimePickerDueDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerDueDate.TabIndex = 184;
            this.dateTimePickerDueDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerDueDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerClaimDate
            // 
            this.dateTimePickerClaimDate.CustomFormat = " ";
            this.dateTimePickerClaimDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerClaimDate.Location = new System.Drawing.Point(359, 14);
            this.dateTimePickerClaimDate.Name = "dateTimePickerClaimDate";
            this.dateTimePickerClaimDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerClaimDate.TabIndex = 183;
            this.dateTimePickerClaimDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerClaimDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerReturningFileDate
            // 
            this.dateTimePickerReturningFileDate.CustomFormat = " ";
            this.dateTimePickerReturningFileDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerReturningFileDate.Location = new System.Drawing.Point(124, 251);
            this.dateTimePickerReturningFileDate.Name = "dateTimePickerReturningFileDate";
            this.dateTimePickerReturningFileDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerReturningFileDate.TabIndex = 182;
            this.dateTimePickerReturningFileDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerReturningFileDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerCourtFeeRefundRequestDate
            // 
            this.dateTimePickerCourtFeeRefundRequestDate.CustomFormat = " ";
            this.dateTimePickerCourtFeeRefundRequestDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerCourtFeeRefundRequestDate.Location = new System.Drawing.Point(124, 227);
            this.dateTimePickerCourtFeeRefundRequestDate.Name = "dateTimePickerCourtFeeRefundRequestDate";
            this.dateTimePickerCourtFeeRefundRequestDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerCourtFeeRefundRequestDate.TabIndex = 181;
            this.dateTimePickerCourtFeeRefundRequestDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerCourtFeeRefundRequestDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerLodgingAppealDate
            // 
            this.dateTimePickerLodgingAppealDate.CustomFormat = " ";
            this.dateTimePickerLodgingAppealDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerLodgingAppealDate.Location = new System.Drawing.Point(124, 203);
            this.dateTimePickerLodgingAppealDate.Name = "dateTimePickerLodgingAppealDate";
            this.dateTimePickerLodgingAppealDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerLodgingAppealDate.TabIndex = 180;
            this.dateTimePickerLodgingAppealDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerLodgingAppealDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerApplyingPetitionDate
            // 
            this.dateTimePickerApplyingPetitionDate.CustomFormat = " ";
            this.dateTimePickerApplyingPetitionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerApplyingPetitionDate.Location = new System.Drawing.Point(124, 179);
            this.dateTimePickerApplyingPetitionDate.Name = "dateTimePickerApplyingPetitionDate";
            this.dateTimePickerApplyingPetitionDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerApplyingPetitionDate.TabIndex = 179;
            this.dateTimePickerApplyingPetitionDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerApplyingPetitionDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickJudgementExecutationDate
            // 
            this.dateTimePickJudgementExecutationDate.CustomFormat = " ";
            this.dateTimePickJudgementExecutationDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickJudgementExecutationDate.Location = new System.Drawing.Point(124, 156);
            this.dateTimePickJudgementExecutationDate.Name = "dateTimePickJudgementExecutationDate";
            this.dateTimePickJudgementExecutationDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickJudgementExecutationDate.TabIndex = 178;
            this.dateTimePickJudgementExecutationDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickJudgementExecutationDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerIssuingWarrantDate
            // 
            this.dateTimePickerIssuingWarrantDate.CustomFormat = " ";
            this.dateTimePickerIssuingWarrantDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerIssuingWarrantDate.Location = new System.Drawing.Point(124, 132);
            this.dateTimePickerIssuingWarrantDate.Name = "dateTimePickerIssuingWarrantDate";
            this.dateTimePickerIssuingWarrantDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerIssuingWarrantDate.TabIndex = 177;
            this.dateTimePickerIssuingWarrantDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerIssuingWarrantDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerMandatoryDate
            // 
            this.dateTimePickerMandatoryDate.CustomFormat = " ";
            this.dateTimePickerMandatoryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerMandatoryDate.Location = new System.Drawing.Point(124, 108);
            this.dateTimePickerMandatoryDate.Name = "dateTimePickerMandatoryDate";
            this.dateTimePickerMandatoryDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerMandatoryDate.TabIndex = 176;
            this.dateTimePickerMandatoryDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerMandatoryDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerDayOfJudgment
            // 
            this.dateTimePickerDayOfJudgment.CustomFormat = " ";
            this.dateTimePickerDayOfJudgment.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDayOfJudgment.Location = new System.Drawing.Point(124, 84);
            this.dateTimePickerDayOfJudgment.Name = "dateTimePickerDayOfJudgment";
            this.dateTimePickerDayOfJudgment.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerDayOfJudgment.TabIndex = 175;
            this.dateTimePickerDayOfJudgment.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerDayOfJudgment.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerFilingDate
            // 
            this.dateTimePickerFilingDate.CustomFormat = " ";
            this.dateTimePickerFilingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFilingDate.Location = new System.Drawing.Point(124, 60);
            this.dateTimePickerFilingDate.Name = "dateTimePickerFilingDate";
            this.dateTimePickerFilingDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerFilingDate.TabIndex = 174;
            this.dateTimePickerFilingDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerFilingDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerNoticeDate
            // 
            this.dateTimePickerNoticeDate.CustomFormat = " ";
            this.dateTimePickerNoticeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerNoticeDate.Location = new System.Drawing.Point(124, 37);
            this.dateTimePickerNoticeDate.Name = "dateTimePickerNoticeDate";
            this.dateTimePickerNoticeDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerNoticeDate.TabIndex = 173;
            this.dateTimePickerNoticeDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerNoticeDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // dateTimePickerAdmissionDate
            // 
            this.dateTimePickerAdmissionDate.CustomFormat = " ";
            this.dateTimePickerAdmissionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerAdmissionDate.Location = new System.Drawing.Point(124, 14);
            this.dateTimePickerAdmissionDate.Name = "dateTimePickerAdmissionDate";
            this.dateTimePickerAdmissionDate.Size = new System.Drawing.Size(100, 20);
            this.dateTimePickerAdmissionDate.TabIndex = 73;
            this.dateTimePickerAdmissionDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.dateTimePickerAdmissionDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(1073, 277);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(24, 16);
            this.label84.TabIndex = 170;
            this.label84.Text = "ง.3";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(944, 277);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(24, 16);
            this.label83.TabIndex = 168;
            this.label83.Text = "ง.2";
            // 
            // comboBoxUnnameDropDown3
            // 
            this.comboBoxUnnameDropDown3.FormattingEnabled = true;
            this.comboBoxUnnameDropDown3.Location = new System.Drawing.Point(714, 323);
            this.comboBoxUnnameDropDown3.Name = "comboBoxUnnameDropDown3";
            this.comboBoxUnnameDropDown3.Size = new System.Drawing.Size(78, 21);
            this.comboBoxUnnameDropDown3.TabIndex = 67;
            this.comboBoxUnnameDropDown3.Text = "X";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(738, 300);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(96, 16);
            this.label82.TabIndex = 165;
            this.label82.Text = "วันเบิกอายัดการขาย";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(728, 277);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(106, 16);
            this.label81.TabIndex = 164;
            this.label81.Text = "วันเบิกขอรับชำระ ง.1";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(734, 253);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(100, 16);
            this.label80.TabIndex = 163;
            this.label80.Text = "วันเบิกเฉลี่ยเงินอายัด";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(1019, 181);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(78, 16);
            this.label79.TabIndex = 160;
            this.label79.Text = "วันเบิกสวมสิทธิ";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(726, 177);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(55, 16);
            this.label78.TabIndex = 156;
            this.label78.Text = "วันอนุญาต";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(1047, 158);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(50, 16);
            this.label77.TabIndex = 155;
            this.label77.Text = "เบิกงวด2";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(895, 158);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(50, 16);
            this.label76.TabIndex = 154;
            this.label76.Text = "เบิกงวด1";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(726, 158);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(55, 16);
            this.label75.TabIndex = 153;
            this.label75.Text = "วันอนุญาต";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(1009, 134);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(88, 16);
            this.label74.TabIndex = 148;
            this.label74.Text = "เบิกเงิน2ยึดทรัพย์";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(990, 110);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(107, 16);
            this.label73.TabIndex = 147;
            this.label73.Text = "งวด2บังคับคดี(อายัด)";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(746, 134);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(88, 16);
            this.label72.TabIndex = 146;
            this.label72.Text = "เบิกเงิน1ยึดทรัพย์";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(727, 110);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(107, 16);
            this.label71.TabIndex = 145;
            this.label71.Text = "งวด1บังคับคดี(อายัด)";
            // 
            // textBoxDocumentNumber
            // 
            this.textBoxDocumentNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDocumentNumber.Location = new System.Drawing.Point(840, 83);
            this.textBoxDocumentNumber.Name = "textBoxDocumentNumber";
            this.textBoxDocumentNumber.Size = new System.Drawing.Size(224, 22);
            this.textBoxDocumentNumber.TabIndex = 144;
            this.textBoxDocumentNumber.Text = "X";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(786, 86);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(48, 16);
            this.label70.TabIndex = 143;
            this.label70.Text = "เลขที่เก็บ";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(991, 62);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(106, 16);
            this.label69.TabIndex = 140;
            this.label69.Text = "การกำหนดงดการขาย";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(1003, 39);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(94, 16);
            this.label68.TabIndex = 139;
            this.label68.Text = "พ้นระยะเวลาบังคับ";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(1011, 16);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(86, 16);
            this.label67.TabIndex = 137;
            this.label67.Text = "วันรับ น.ส. มอบฯ";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(754, 16);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(80, 16);
            this.label66.TabIndex = 135;
            this.label66.Text = "วันส่ง น.ส. มอบ";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(530, 325);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(78, 16);
            this.label65.TabIndex = 124;
            this.label65.Text = "วันพิทักษ์ทรัพย์";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(511, 300);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(97, 16);
            this.label64.TabIndex = 123;
            this.label64.Text = "วันอายัดจากการขาย";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(524, 277);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(84, 16);
            this.label63.TabIndex = 122;
            this.label63.Text = "วันยื่นขอรับชำระ";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(524, 253);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(84, 16);
            this.label62.TabIndex = 121;
            this.label62.Text = "วันเฉลี่ยเงินอายัด";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(533, 181);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(75, 16);
            this.label61.TabIndex = 120;
            this.label61.Text = "วันยื่นสวมสิทธิ์";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(523, 158);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(85, 16);
            this.label60.TabIndex = 119;
            this.label60.Text = "วันยื่นเฉลี่ยทรัพย์";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(546, 134);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(62, 16);
            this.label59.TabIndex = 118;
            this.label59.Text = "วันยึดทรัพย์";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(514, 110);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(94, 16);
            this.label58.TabIndex = 117;
            this.label58.Text = "วันที่ตั้งเครื่องอายัด";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(524, 39);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(84, 16);
            this.label56.TabIndex = 116;
            this.label56.Text = "รับเรื่องสืบทรัพย์";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(515, 16);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(93, 16);
            this.label57.TabIndex = 115;
            this.label57.Text = "วันรับงานบังคับคดี";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(515, 229);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 16);
            this.label55.TabIndex = 114;
            this.label55.Text = "บาท";
            // 
            // textBoxCourtChequeAmount
            // 
            this.textBoxCourtChequeAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCourtChequeAmount.Location = new System.Drawing.Point(459, 226);
            this.textBoxCourtChequeAmount.Name = "textBoxCourtChequeAmount";
            this.textBoxCourtChequeAmount.Size = new System.Drawing.Size(52, 22);
            this.textBoxCourtChequeAmount.TabIndex = 113;
            this.textBoxCourtChequeAmount.Text = "1500";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(713, 86);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 16);
            this.label54.TabIndex = 112;
            this.label54.Text = "บาท";
            // 
            // textBoxCourtFeeRefundAmount
            // 
            this.textBoxCourtFeeRefundAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCourtFeeRefundAmount.Location = new System.Drawing.Point(657, 83);
            this.textBoxCourtFeeRefundAmount.Name = "textBoxCourtFeeRefundAmount";
            this.textBoxCourtFeeRefundAmount.Size = new System.Drawing.Size(52, 22);
            this.textBoxCourtFeeRefundAmount.TabIndex = 111;
            this.textBoxCourtFeeRefundAmount.Text = "X";
            // 
            // checkBoxUnnameCheckBox
            // 
            this.checkBoxUnnameCheckBox.AutoSize = true;
            this.checkBoxUnnameCheckBox.Location = new System.Drawing.Point(537, 86);
            this.checkBoxUnnameCheckBox.Name = "checkBoxUnnameCheckBox";
            this.checkBoxUnnameCheckBox.Size = new System.Drawing.Size(122, 17);
            this.checkBoxUnnameCheckBox.TabIndex = 110;
            this.checkBoxUnnameCheckBox.Text = "พับ / ค่าฤชา + ทนาย";
            this.checkBoxUnnameCheckBox.UseVisualStyleBackColor = true;
            // 
            // textBoxCourtFee
            // 
            this.textBoxCourtFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCourtFee.Location = new System.Drawing.Point(359, 59);
            this.textBoxCourtFee.Name = "textBoxCourtFee";
            this.textBoxCourtFee.Size = new System.Drawing.Size(52, 22);
            this.textBoxCourtFee.TabIndex = 102;
            this.textBoxCourtFee.Text = "0";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(274, 253);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(77, 16);
            this.label53.TabIndex = 99;
            this.label53.Text = "วันสั่งคืนเช็ค ธ.";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(282, 229);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(69, 16);
            this.label52.TabIndex = 98;
            this.label52.Text = "วันรับเช็คศาล";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(248, 205);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(103, 16);
            this.label51.TabIndex = 97;
            this.label51.Text = "วันที่พิพากษาอุทธรณ์";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(269, 181);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(82, 16);
            this.label50.TabIndex = 96;
            this.label50.Text = "วันรับผลอุทธรณ์";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(274, 158);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(77, 16);
            this.label49.TabIndex = 95;
            this.label49.Text = "วันที่คดีถึงที่สุด";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(291, 134);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(60, 16);
            this.label48.TabIndex = 94;
            this.label48.Text = "วันถอดฟ้อง";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(269, 110);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(82, 16);
            this.label47.TabIndex = 93;
            this.label47.Text = "วันที่ปิดคำบังคับ";
            // 
            // comboBoxUnnameDropDown1
            // 
            this.comboBoxUnnameDropDown1.FormattingEnabled = true;
            this.comboBoxUnnameDropDown1.Location = new System.Drawing.Point(229, 84);
            this.comboBoxUnnameDropDown1.Name = "comboBoxUnnameDropDown1";
            this.comboBoxUnnameDropDown1.Size = new System.Drawing.Size(96, 21);
            this.comboBoxUnnameDropDown1.TabIndex = 67;
            this.comboBoxUnnameDropDown1.Text = "X";
            // 
            // comboBoxUnnameDropDown2
            // 
            this.comboBoxUnnameDropDown2.FormattingEnabled = true;
            this.comboBoxUnnameDropDown2.Location = new System.Drawing.Point(329, 84);
            this.comboBoxUnnameDropDown2.Name = "comboBoxUnnameDropDown2";
            this.comboBoxUnnameDropDown2.Size = new System.Drawing.Size(204, 21);
            this.comboBoxUnnameDropDown2.TabIndex = 68;
            this.comboBoxUnnameDropDown2.Text = "X";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(299, 62);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(52, 16);
            this.label46.TabIndex = 92;
            this.label46.Text = "ค่าขึ้นศาล";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(269, 39);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(82, 16);
            this.label44.TabIndex = 91;
            this.label44.Text = "วันที่ครบกำหนด";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(269, 16);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(82, 16);
            this.label45.TabIndex = 90;
            this.label45.Text = "กำหนดวันที่ฟ้อง";
            // 
            // textBoxAdditionDetail
            // 
            this.textBoxAdditionDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxAdditionDetail.Location = new System.Drawing.Point(124, 274);
            this.textBoxAdditionDetail.Name = "textBoxAdditionDetail";
            this.textBoxAdditionDetail.Size = new System.Drawing.Size(392, 22);
            this.textBoxAdditionDetail.TabIndex = 86;
            this.textBoxAdditionDetail.Text = "X";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(64, 277);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(56, 16);
            this.label43.TabIndex = 78;
            this.label43.Text = "บันทึกอื่นๆ";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(32, 253);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(89, 16);
            this.label42.TabIndex = 77;
            this.label42.Text = "วันที่คืนเอกสาร ช.";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(28, 229);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(92, 16);
            this.label41.TabIndex = 76;
            this.label41.Text = "วันขอค้นคำขึ้นศาล";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(43, 205);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(77, 16);
            this.label40.TabIndex = 75;
            this.label40.Text = "วันที่ยื่นอุทธรณ์";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(17, 181);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(103, 16);
            this.label39.TabIndex = 74;
            this.label39.Text = "วันส่งหนังสืออุทธรณ์";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(12, 158);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(108, 16);
            this.label38.TabIndex = 73;
            this.label38.Text = "วันที่ในหมายบังคับคดี";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(23, 134);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(97, 16);
            this.label37.TabIndex = 72;
            this.label37.Text = "วันขอออกหมายตั้งฯ";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(42, 110);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(78, 16);
            this.label36.TabIndex = 71;
            this.label36.Text = "วันออกคำบังคับ";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(60, 86);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(60, 16);
            this.label35.TabIndex = 70;
            this.label35.Text = "วันพิพากษา";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(79, 62);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 16);
            this.label34.TabIndex = 69;
            this.label34.Text = "วันฟ้อง";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(10, 39);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(110, 16);
            this.label33.TabIndex = 68;
            this.label33.Text = "วันส่งหนังสือบอกกล่าว";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(45, 16);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(75, 16);
            this.label32.TabIndex = 67;
            this.label32.Text = "วันรับเรื่องฟ้อง";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBoxJudgementResult);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1208, 421);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ผลคำพิพากษา";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBoxJudgementResult
            // 
            this.textBoxJudgementResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxJudgementResult.Location = new System.Drawing.Point(4, 4);
            this.textBoxJudgementResult.Multiline = true;
            this.textBoxJudgementResult.Name = "textBoxJudgementResult";
            this.textBoxJudgementResult.Size = new System.Drawing.Size(1200, 364);
            this.textBoxJudgementResult.TabIndex = 69;
            this.textBoxJudgementResult.Text = "X";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.textBoxDebtDetail);
            this.tabPage4.Controls.Add(this.dataGridView1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1208, 421);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ภาระหนี้";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // textBoxDebtDetail
            // 
            this.textBoxDebtDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxDebtDetail.Location = new System.Drawing.Point(4, 4);
            this.textBoxDebtDetail.Multiline = true;
            this.textBoxDebtDetail.Name = "textBoxDebtDetail";
            this.textBoxDebtDetail.Size = new System.Drawing.Size(1200, 412);
            this.textBoxDebtDetail.TabIndex = 70;
            this.textBoxDebtDetail.Text = "X";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number,
            this.DebtGroup,
            this.RegistrationNumber,
            this.Type,
            this.ContractNo,
            this.ContractDate,
            this.CreditAmount,
            this.TotalDebtAmount,
            this.BSY,
            this.Borrower,
            this.Manager,
            this.Guarantor,
            this.Condition});
            this.dataGridView1.Location = new System.Drawing.Point(4, 260);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1200, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Visible = false;
            // 
            // Number
            // 
            this.Number.HeaderText = "ลำดับ";
            this.Number.Name = "Number";
            // 
            // DebtGroup
            // 
            this.DebtGroup.HeaderText = "กลุ่มหนี้";
            this.DebtGroup.Name = "DebtGroup";
            // 
            // RegistrationNumber
            // 
            this.RegistrationNumber.HeaderText = "ทะเบียน ล.";
            this.RegistrationNumber.Name = "RegistrationNumber";
            // 
            // Type
            // 
            this.Type.HeaderText = "ประเภท";
            this.Type.Name = "Type";
            // 
            // ContractNo
            // 
            this.ContractNo.HeaderText = "เลขสัญญา/บัตร";
            this.ContractNo.Name = "ContractNo";
            // 
            // ContractDate
            // 
            this.ContractDate.HeaderText = "วันทำสัญญา";
            this.ContractDate.Name = "ContractDate";
            // 
            // CreditAmount
            // 
            this.CreditAmount.HeaderText = "วงเงิน";
            this.CreditAmount.Name = "CreditAmount";
            // 
            // TotalDebtAmount
            // 
            this.TotalDebtAmount.HeaderText = "ยอดหนี้รวม";
            this.TotalDebtAmount.Name = "TotalDebtAmount";
            // 
            // BSY
            // 
            this.BSY.HeaderText = "บสย";
            this.BSY.Name = "BSY";
            // 
            // Borrower
            // 
            this.Borrower.HeaderText = "ผู้กู้";
            this.Borrower.Name = "Borrower";
            // 
            // Manager
            // 
            this.Manager.HeaderText = "ผจก.(หจก.)";
            this.Manager.Name = "Manager";
            // 
            // Guarantor
            // 
            this.Guarantor.HeaderText = "ผู้ค้ำ";
            this.Guarantor.Name = "Guarantor";
            // 
            // Condition
            // 
            this.Condition.HeaderText = "เงื่อนไข";
            this.Condition.Name = "Condition";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.textBoxLitigateDetail);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1208, 421);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "รายงานดำเนินคดี";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // textBoxLitigateDetail
            // 
            this.textBoxLitigateDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxLitigateDetail.Location = new System.Drawing.Point(4, 3);
            this.textBoxLitigateDetail.Multiline = true;
            this.textBoxLitigateDetail.Name = "textBoxLitigateDetail";
            this.textBoxLitigateDetail.Size = new System.Drawing.Size(1200, 413);
            this.textBoxLitigateDetail.TabIndex = 70;
            this.textBoxLitigateDetail.Text = "X";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.textBoxOperationRegister);
            this.tabPage6.Controls.Add(this.dataGridView2);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1208, 421);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "ปฏิบัติงาน/ระเบียนงาน";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // textBoxOperationRegister
            // 
            this.textBoxOperationRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxOperationRegister.Location = new System.Drawing.Point(4, 4);
            this.textBoxOperationRegister.Multiline = true;
            this.textBoxOperationRegister.Name = "textBoxOperationRegister";
            this.textBoxOperationRegister.Size = new System.Drawing.Size(1200, 413);
            this.textBoxOperationRegister.TabIndex = 71;
            this.textBoxOperationRegister.Text = "X";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Detail,
            this.Remark,
            this.Lawyer});
            this.dataGridView2.Location = new System.Drawing.Point(4, 268);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(1200, 150);
            this.dataGridView2.TabIndex = 1;
            this.dataGridView2.Visible = false;
            // 
            // Date
            // 
            this.Date.HeaderText = "วันที่";
            this.Date.Name = "Date";
            // 
            // Detail
            // 
            this.Detail.HeaderText = "รายละเอียด";
            this.Detail.Name = "Detail";
            // 
            // Remark
            // 
            this.Remark.HeaderText = "หมายเหตุ";
            this.Remark.Name = "Remark";
            // 
            // Lawyer
            // 
            this.Lawyer.HeaderText = "ทนายความ";
            this.Lawyer.Name = "Lawyer";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.dataGridView3);
            this.tabPage7.Controls.Add(this.button1);
            this.tabPage7.Controls.Add(this.label106);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1208, 421);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "เอกสาร";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(144, 20);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(240, 150);
            this.dataGridView3.TabIndex = 79;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.button1.Location = new System.Drawing.Point(20, 40);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 32);
            this.button1.TabIndex = 78;
            this.button1.Text = "เพิ่มเอกสาร";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(20, 20);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(75, 16);
            this.label106.TabIndex = 78;
            this.label106.Text = "เอกสาร Scan";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.label107);
            this.tabPage8.Controls.Add(this.pictureBox1);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(1208, 421);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "ชุดแบบฟ้องคดี";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label107.ForeColor = System.Drawing.Color.LightSalmon;
            this.label107.Location = new System.Drawing.Point(956, 176);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(169, 20);
            this.label107.TabIndex = 78;
            this.label107.Text = "Picture Only for now";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(916, 412);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(435, 71);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 16);
            this.label14.TabIndex = 32;
            this.label14.Text = "ขาดอายุความ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(456, 94);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 16);
            this.label15.TabIndex = 33;
            this.label15.Text = "กล่องเก็บ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(426, 139);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 16);
            this.label16.TabIndex = 34;
            this.label16.Text = "ยอดหนี้ ณ วันที่";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(466, 164);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 16);
            this.label17.TabIndex = 35;
            this.label17.Text = "เงินต้น";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(461, 187);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 16);
            this.label18.TabIndex = 36;
            this.label18.Text = "ดอกเบี้ย";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(453, 212);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 16);
            this.label19.TabIndex = 37;
            this.label19.Text = "ทุนทรัพย์";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox1.Location = new System.Drawing.Point(508, 88);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(96, 22);
            this.textBox1.TabIndex = 40;
            this.textBox1.Text = "X";
            // 
            // textBoxCourtLocation
            // 
            this.textBoxCourtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCourtLocation.Location = new System.Drawing.Point(400, 112);
            this.textBoxCourtLocation.Name = "textBoxCourtLocation";
            this.textBoxCourtLocation.Size = new System.Drawing.Size(204, 22);
            this.textBoxCourtLocation.TabIndex = 41;
            this.textBoxCourtLocation.Visible = false;
            // 
            // textBoxCapital
            // 
            this.textBoxCapital.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCapital.Location = new System.Drawing.Point(508, 160);
            this.textBoxCapital.Name = "textBoxCapital";
            this.textBoxCapital.Size = new System.Drawing.Size(96, 22);
            this.textBoxCapital.TabIndex = 43;
            this.textBoxCapital.Text = "X";
            // 
            // textBoxInterest
            // 
            this.textBoxInterest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxInterest.Location = new System.Drawing.Point(508, 184);
            this.textBoxInterest.Name = "textBoxInterest";
            this.textBoxInterest.Size = new System.Drawing.Size(96, 22);
            this.textBoxInterest.TabIndex = 44;
            this.textBoxInterest.Text = "X";
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxTotal.Location = new System.Drawing.Point(508, 208);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.Size = new System.Drawing.Size(96, 22);
            this.textBoxTotal.TabIndex = 45;
            this.textBoxTotal.Text = "X";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(623, 186);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 16);
            this.label20.TabIndex = 50;
            this.label20.Text = "Type";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(604, 212);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 16);
            this.label21.TabIndex = 51;
            this.label21.Text = "สถานะฟ้อง";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(604, 236);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(113, 16);
            this.label22.TabIndex = 52;
            this.label22.Text = "วันที่ปิดแฟ้มงาน (ฟ้อง)";
            // 
            // comboBoxProsecutorType
            // 
            this.comboBoxProsecutorType.FormattingEnabled = true;
            this.comboBoxProsecutorType.Location = new System.Drawing.Point(664, 184);
            this.comboBoxProsecutorType.Name = "comboBoxProsecutorType";
            this.comboBoxProsecutorType.Size = new System.Drawing.Size(236, 21);
            this.comboBoxProsecutorType.TabIndex = 53;
            this.comboBoxProsecutorType.Text = "X";
            // 
            // comboBoxSueStatus
            // 
            this.comboBoxSueStatus.FormattingEnabled = true;
            this.comboBoxSueStatus.Location = new System.Drawing.Point(664, 208);
            this.comboBoxSueStatus.Name = "comboBoxSueStatus";
            this.comboBoxSueStatus.Size = new System.Drawing.Size(104, 21);
            this.comboBoxSueStatus.TabIndex = 54;
            this.comboBoxSueStatus.Text = "X";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(940, 44);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 16);
            this.label23.TabIndex = 56;
            this.label23.Text = "Work List";
            this.label23.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(940, 64);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(163, 16);
            this.label24.TabIndex = 57;
            this.label24.Text = "คดีที่ยังไม่ได้คัดคำพิพากษา (644)";
            this.label24.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(940, 84);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(179, 16);
            this.label25.TabIndex = 58;
            this.label25.Text = "คดีที่ใกล้กำหนดงดการขาย 90 วัน (1)";
            this.label25.Visible = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(940, 104);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(84, 16);
            this.label26.TabIndex = 59;
            this.label26.Text = "คดีพิทักทัพย์ (6)";
            this.label26.Visible = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(940, 124);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(196, 16);
            this.label27.TabIndex = 60;
            this.label27.Text = "ครบกำหนดออกหมายตั้งเจ้าพนักงาน (28)";
            this.label27.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(940, 144);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(197, 16);
            this.label28.TabIndex = 61;
            this.label28.Text = "คัดเอกสารออกหมายตั้งเจ้าพนักงาน (484)";
            this.label28.Visible = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(940, 164);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(96, 16);
            this.label29.TabIndex = 62;
            this.label29.Text = "เตือนนัดหมาย (17)";
            this.label29.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(940, 184);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(111, 16);
            this.label30.TabIndex = 63;
            this.label30.Text = "ใกล้ขาดอายุความ (39)";
            this.label30.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(768, 212);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(49, 16);
            this.label31.TabIndex = 64;
            this.label31.Text = "บังคับคดี";
            // 
            // comboBoxExecution
            // 
            this.comboBoxExecution.FormattingEnabled = true;
            this.comboBoxExecution.Location = new System.Drawing.Point(820, 208);
            this.comboBoxExecution.Name = "comboBoxExecution";
            this.comboBoxExecution.Size = new System.Drawing.Size(78, 21);
            this.comboBoxExecution.TabIndex = 65;
            this.comboBoxExecution.Text = "X";
            // 
            // comboBoxFileStatus
            // 
            this.comboBoxFileStatus.FormattingEnabled = true;
            this.comboBoxFileStatus.Location = new System.Drawing.Point(820, 232);
            this.comboBoxFileStatus.Name = "comboBoxFileStatus";
            this.comboBoxFileStatus.Size = new System.Drawing.Size(78, 21);
            this.comboBoxFileStatus.TabIndex = 66;
            this.comboBoxFileStatus.Text = "X";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // btnUpload
            // 
            this.btnUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnUpload.ForeColor = System.Drawing.Color.SpringGreen;
            this.btnUpload.Location = new System.Drawing.Point(1152, 64);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(48, 28);
            this.btnUpload.TabIndex = 67;
            this.btnUpload.Text = "UP";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Visible = false;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnSave.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.btnSave.Location = new System.Drawing.Point(937, 224);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 32);
            this.btnSave.TabIndex = 68;
            this.btnSave.Text = "เพิ่ม";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // textBoxPrescriptionPrecludedDate
            // 
            this.textBoxPrescriptionPrecludedDate.CustomFormat = " ";
            this.textBoxPrescriptionPrecludedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.textBoxPrescriptionPrecludedDate.Location = new System.Drawing.Point(504, 68);
            this.textBoxPrescriptionPrecludedDate.Name = "textBoxPrescriptionPrecludedDate";
            this.textBoxPrescriptionPrecludedDate.Size = new System.Drawing.Size(100, 20);
            this.textBoxPrescriptionPrecludedDate.TabIndex = 69;
            this.textBoxPrescriptionPrecludedDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.textBoxPrescriptionPrecludedDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // textBoxDebtRecordDate
            // 
            this.textBoxDebtRecordDate.CustomFormat = " ";
            this.textBoxDebtRecordDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.textBoxDebtRecordDate.Location = new System.Drawing.Point(504, 136);
            this.textBoxDebtRecordDate.Name = "textBoxDebtRecordDate";
            this.textBoxDebtRecordDate.Size = new System.Drawing.Size(100, 20);
            this.textBoxDebtRecordDate.TabIndex = 70;
            this.textBoxDebtRecordDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.textBoxDebtRecordDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // textBoxCloseDate
            // 
            this.textBoxCloseDate.CustomFormat = " ";
            this.textBoxCloseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.textBoxCloseDate.Location = new System.Drawing.Point(720, 232);
            this.textBoxCloseDate.Name = "textBoxCloseDate";
            this.textBoxCloseDate.Size = new System.Drawing.Size(100, 20);
            this.textBoxCloseDate.TabIndex = 71;
            this.textBoxCloseDate.ValueChanged += new System.EventHandler(this.DateTimePickerValueChanged);
            this.textBoxCloseDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClearDateTimePickerValue);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnUpdate.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnUpdate.Location = new System.Drawing.Point(1018, 224);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 32);
            this.btnUpdate.TabIndex = 72;
            this.btnUpdate.Text = "อัพเดท";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // textBoxSearchFileNo2
            // 
            this.textBoxSearchFileNo2.Location = new System.Drawing.Point(714, 40);
            this.textBoxSearchFileNo2.Name = "textBoxSearchFileNo2";
            this.textBoxSearchFileNo2.Size = new System.Drawing.Size(100, 20);
            this.textBoxSearchFileNo2.TabIndex = 73;
            this.textBoxSearchFileNo2.Text = "X";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(634, 41);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(53, 16);
            this.label105.TabIndex = 74;
            this.label105.Text = "เลขที่แฟ้ม";
            // 
            // textBoxSearchFileNo1
            // 
            this.textBoxSearchFileNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxSearchFileNo1.Location = new System.Drawing.Point(684, 40);
            this.textBoxSearchFileNo1.Name = "textBoxSearchFileNo1";
            this.textBoxSearchFileNo1.Size = new System.Drawing.Size(24, 22);
            this.textBoxSearchFileNo1.TabIndex = 75;
            this.textBoxSearchFileNo1.Text = "X";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnSearch.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnSearch.Location = new System.Drawing.Point(820, 32);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 32);
            this.btnSearch.TabIndex = 76;
            this.btnSearch.Text = "ค้นหา";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dataGridViewCase
            // 
            this.dataGridViewCase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.View,
            this.AdmissionDate,
            this.FileNo1,
            this.FileNo2});
            this.dataGridViewCase.Location = new System.Drawing.Point(626, 71);
            this.dataGridViewCase.Name = "dataGridViewCase";
            this.dataGridViewCase.Size = new System.Drawing.Size(375, 112);
            this.dataGridViewCase.TabIndex = 77;
            this.dataGridViewCase.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCase_CellContentClick);
            // 
            // View
            // 
            this.View.HeaderText = "ดู";
            this.View.Name = "View";
            this.View.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.View.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.View.Text = "ดู";
            this.View.ToolTipText = "ดู";
            this.View.UseColumnTextForButtonValue = true;
            this.View.Width = 30;
            // 
            // AdmissionDate
            // 
            this.AdmissionDate.DataPropertyName = "AdmissionDate";
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            this.AdmissionDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.AdmissionDate.HeaderText = "วันรับเรื่องฟ้อง";
            this.AdmissionDate.Name = "AdmissionDate";
            // 
            // FileNo1
            // 
            this.FileNo1.DataPropertyName = "FileNo1";
            this.FileNo1.HeaderText = "เลขที่แฟ้มหน้า";
            this.FileNo1.Name = "FileNo1";
            // 
            // FileNo2
            // 
            this.FileNo2.DataPropertyName = "FileNo2";
            this.FileNo2.HeaderText = "เลขที่แฟ้มหลัง";
            this.FileNo2.Name = "FileNo2";
            // 
            // comboBoxCaseType
            // 
            this.comboBoxCaseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCaseType.FormattingEnabled = true;
            this.comboBoxCaseType.Items.AddRange(new object[] {
            "แพ่ง",
            "อาญา"});
            this.comboBoxCaseType.Location = new System.Drawing.Point(80, 135);
            this.comboBoxCaseType.Name = "comboBoxCaseType";
            this.comboBoxCaseType.Size = new System.Drawing.Size(56, 21);
            this.comboBoxCaseType.TabIndex = 79;
            // 
            // textBoxFileNo2
            // 
            this.textBoxFileNo2.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.textBoxFileNo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxFileNo2.Location = new System.Drawing.Point(108, 38);
            this.textBoxFileNo2.Name = "textBoxFileNo2";
            this.textBoxFileNo2.Size = new System.Drawing.Size(96, 22);
            this.textBoxFileNo2.TabIndex = 82;
            this.textBoxFileNo2.Text = "X";
            // 
            // textBoxFileNo1
            // 
            this.textBoxFileNo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxFileNo1.Location = new System.Drawing.Point(84, 38);
            this.textBoxFileNo1.Name = "textBoxFileNo1";
            this.textBoxFileNo1.Size = new System.Drawing.Size(24, 22);
            this.textBoxFileNo1.TabIndex = 81;
            this.textBoxFileNo1.Text = "X";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 80;
            this.label1.Text = "เลขที่แฟ้ม";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 722);
            this.Controls.Add(this.textBoxFileNo2);
            this.Controls.Add(this.textBoxFileNo1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxCaseType);
            this.Controls.Add(this.dataGridViewCase);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.textBoxSearchFileNo1);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.textBoxSearchFileNo2);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.textBoxCloseDate);
            this.Controls.Add(this.textBoxDebtRecordDate);
            this.Controls.Add(this.textBoxPrescriptionPrecludedDate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.comboBoxFileStatus);
            this.Controls.Add(this.comboBoxExecution);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.comboBoxSueStatus);
            this.Controls.Add(this.comboBoxProsecutorType);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBoxTotal);
            this.Controls.Add(this.textBoxInterest);
            this.Controls.Add(this.textBoxCapital);
            this.Controls.Add(this.textBoxCourtLocation);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.textBoxDefendantRemark3);
            this.Controls.Add(this.textBoxDefendantRemark2);
            this.Controls.Add(this.textBoxDefendantRemark1);
            this.Controls.Add(this.comboBoxDefendant);
            this.Controls.Add(this.comboBoxComplainant);
            this.Controls.Add(this.comboBoxProsecutor);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.comboBoxAllegationType);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxCourtName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxRedCaseNo);
            this.Controls.Add(this.textBoxBlackCaseNo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxAccountNo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxCaseNo2);
            this.Controls.Add(this.textBoxCaseNo1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxCompletedFileNo);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.Text = "บริษัท ศราภูมิ ลีเกิล คอนซัลแท็นต์ จำกัด";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCase)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCompletedFileNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxCaseNo2;
        private System.Windows.Forms.TextBox textBoxCaseNo1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAccountNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxBlackCaseNo;
        private System.Windows.Forms.TextBox textBoxRedCaseNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxCourtName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxAllegationType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxProsecutor;
        private System.Windows.Forms.ComboBox comboBoxComplainant;
        private System.Windows.Forms.ComboBox comboBoxDefendant;
        private System.Windows.Forms.TextBox textBoxDefendantRemark1;
        private System.Windows.Forms.TextBox textBoxDefendantRemark2;
        private System.Windows.Forms.TextBox textBoxDefendantRemark3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBoxCourtLocation;
        private System.Windows.Forms.TextBox textBoxCapital;
        private System.Windows.Forms.TextBox textBoxInterest;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.ComboBox comboBoxUnnameDropDown3;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox textBoxDocumentNumber;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBoxCourtChequeAmount;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBoxCourtFeeRefundAmount;
        private System.Windows.Forms.CheckBox checkBoxUnnameCheckBox;
        private System.Windows.Forms.TextBox textBoxCourtFee;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox comboBoxUnnameDropDown1;
        private System.Windows.Forms.ComboBox comboBoxUnnameDropDown2;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBoxAdditionDetail;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBoxProsecutorType;
        private System.Windows.Forms.ComboBox comboBoxSueStatus;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox comboBoxExecution;
        private System.Windows.Forms.ComboBox comboBoxFileStatus;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.ComboBox comboBoxCaseOfficer;
        private System.Windows.Forms.ComboBox comboBoxCaseGroup;
        private System.Windows.Forms.ComboBox comboBoxBSY;
        private System.Windows.Forms.ComboBox comboBoxMortgage;
        private System.Windows.Forms.ComboBox comboBoxAssetDetectiveOfficer;
        private System.Windows.Forms.ComboBox comboBoxWitness;
        private System.Windows.Forms.TextBox textBoxCaseWorkRel;
        private System.Windows.Forms.TextBox textBoxOperator;
        private System.Windows.Forms.TextBox textBoxFollowingResult;
        private System.Windows.Forms.TextBox textBoxCallResult;
        private System.Windows.Forms.TextBox textBoxCaseResult;
        private System.Windows.Forms.TextBox textBoxAccountingOfficer;
        private System.Windows.Forms.TextBox textBoxComplaintWorkRel;
        private System.Windows.Forms.TextBox textBoxYellowResult;
        private System.Windows.Forms.TextBox textBoxRemark;
        private System.Windows.Forms.TextBox textBoxDefendantNameList;
        private System.Windows.Forms.TextBox textBoxHelperLawyer;
        private System.Windows.Forms.TextBox textBoxDisburse3;
        private System.Windows.Forms.TextBox textBoxDisburse2;
        private System.Windows.Forms.ComboBox comboBoxAfterSentenceLawyer;
        private System.Windows.Forms.ComboBox comboBoxDetectiveLaywer;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox textBoxDisburse1;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.ComboBox comboBoxComplainantLawyer;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxJudgementResult;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewTextBoxColumn DebtGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegistrationNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContractNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContractDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreditAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalDebtAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn BSY;
        private System.Windows.Forms.DataGridViewTextBoxColumn Borrower;
        private System.Windows.Forms.DataGridViewTextBoxColumn Manager;
        private System.Windows.Forms.DataGridViewTextBoxColumn Guarantor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Condition;
        private System.Windows.Forms.TextBox textBoxLitigateDetail;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lawyer;
        private System.Windows.Forms.DateTimePicker textBoxPrescriptionPrecludedDate;
        private System.Windows.Forms.DateTimePicker textBoxDebtRecordDate;
        private System.Windows.Forms.DateTimePicker textBoxCloseDate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DateTimePicker dateTimePickerPaymentWithdrawalDate3;
        private System.Windows.Forms.DateTimePicker dateTimePickerPaymentWithdrawalDate2;
        private System.Windows.Forms.DateTimePicker dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerPaymentWithdrawalDate1;
        private System.Windows.Forms.DateTimePicker dateTimePickerWithdrawalDividedAttachedFundsDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerPermittedDate2;
        private System.Windows.Forms.DateTimePicker dateTimePickerPermittedDate1;
        private System.Windows.Forms.DateTimePicker dateTimePickerWithdrawalDate1;
        private System.Windows.Forms.DateTimePicker dateTimePickerSubrogatedWithdrawalDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerWithdrawalDate2;
        private System.Windows.Forms.DateTimePicker dateTimePickerWithdrawalAttachment2;
        private System.Windows.Forms.DateTimePicker dateTimePickerExecutationAttachment2;
        private System.Windows.Forms.DateTimePicker dateTimePickerWithdrawalAttachment1;
        private System.Windows.Forms.DateTimePicker dateTimePickerExecutationAttachment1;
        private System.Windows.Forms.DateTimePicker dateTimePickerEndOfSellingAuctionDueDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerExpiredCaseDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAcquiringProxyDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerSendingProxyDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAbsoluteReceivershipDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAttachmentAfterSellingDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerRequestForPaymentDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerDivisionDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerSubrogationDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerApplyingDivisionDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerSeizingPropertiesDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerApplyingAttachmentDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAcquiringSuccessorDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerOpeningCaseDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerReturningChequeDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerCourtChequeDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAppealJudgementDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAcceptingAppealJudgementDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerTerminatingCaseApprovalDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerWithdrawalDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerJudgementExecutationClosingDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerDueDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerClaimDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerReturningFileDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerCourtFeeRefundRequestDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerLodgingAppealDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerApplyingPetitionDate;
        private System.Windows.Forms.DateTimePicker dateTimePickJudgementExecutationDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerIssuingWarrantDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerMandatoryDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerDayOfJudgment;
        private System.Windows.Forms.DateTimePicker dateTimePickerFilingDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerNoticeDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerAdmissionDate;
        private System.Windows.Forms.TextBox textBoxSearchFileNo2;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox textBoxSearchFileNo1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dataGridViewCase;
        private System.Windows.Forms.TextBox textBoxDebtDetail;
        private System.Windows.Forms.TextBox textBoxOperationRegister;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewButtonColumn View;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdmissionDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileNo1;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileNo2;
        private System.Windows.Forms.ComboBox comboBoxCaseType;
        private System.Windows.Forms.TextBox textBoxFileNo2;
        private System.Windows.Forms.TextBox textBoxFileNo1;
        private System.Windows.Forms.Label label1;
    }
}

