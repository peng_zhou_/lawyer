﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lawyer
{
    public partial class Form1 : Form
    {
        Database database = new Database();
        string caseId = "0";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MyListControl.BindComboBox(comboBoxCourtName, database.GetDataTable("USP_GetCourtList"), "");
            MyListControl.BindComboBox(comboBoxAllegationType, database.GetDataTable("USP_GetAllegationTypeList"), "");
            MyListControl.BindComboBox(comboBoxProsecutor, database.GetDataTable("USP_GetProsecutorList"), "");
            MyListControl.BindComboBox(comboBoxComplainant, database.GetDataTable("USP_GetComplainantList"), "");
            MyListControl.BindComboBox(comboBoxDefendant, database.GetDataTable("USP_GetDefendantList"), "");

            MyListControl.BindComboBox(comboBoxComplainantLawyer, database.GetDataTable("USP_GetLawyerList"), "");
            MyListControl.BindComboBox(comboBoxDetectiveLaywer, database.GetDataTable("USP_GetLawyerList"), "");
            MyListControl.BindComboBox(comboBoxAfterSentenceLawyer, database.GetDataTable("USP_GetLawyerList"), "");

            comboBoxCaseType.SelectedIndex = 0;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            Database sqlCon = new Database();
            sqlCon.TestConnection();

            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            MessageBox.Show("Success");
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SqlTransaction sqlTrans = null;
                database.GetSqlConnection().Open();
              sqlTrans =  database.GetSqlConnection().BeginTransaction();

                SqlCommand sqlCmd = new SqlCommand("USP_InsertFile");
                sqlCmd.Transaction = sqlTrans;

                sqlCmd.Parameters.AddWithValue("@FileNo1", textBoxFileNo1.Text);
                sqlCmd.Parameters.AddWithValue("@CompletedFileNo", textBoxCompletedFileNo.Text);
                sqlCmd.Parameters.AddWithValue("@FileNo2", textBoxFileNo2.Text);
                sqlCmd.Parameters.AddWithValue("@CaseNo1", "");//textBoxCaseNo1.Text);
                sqlCmd.Parameters.AddWithValue("@CaseNo2", "");//textBoxCaseNo2.Text);
                sqlCmd.Parameters.AddWithValue("@AccountNo", textBoxAccountNo.Text);
                sqlCmd.Parameters.AddWithValue("@BlackCaseNo", textBoxBlackCaseNo.Text);
                sqlCmd.Parameters.AddWithValue("@RedCaseNo", textBoxRedCaseNo.Text);
                sqlCmd.Parameters.AddWithValue("@CourtName", comboBoxCourtName.Text);
                sqlCmd.Parameters.AddWithValue("@CaseType", comboBoxCaseType.Text);
                sqlCmd.Parameters.AddWithValue("@AllegationType", comboBoxAllegationType.Text);
                sqlCmd.Parameters.AddWithValue("@Prosecutor", comboBoxProsecutor.Text);
                sqlCmd.Parameters.AddWithValue("@Complainant", comboBoxComplainant.Text);
                sqlCmd.Parameters.AddWithValue("@Defendant", comboBoxDefendant.Text);
                sqlCmd.Parameters.AddWithValue("@DefendantRemark1", textBoxDefendantRemark1.Text);
                sqlCmd.Parameters.AddWithValue("@DefendantRemark2", textBoxDefendantRemark2.Text);
                sqlCmd.Parameters.AddWithValue("@DefendantRemark3", textBoxDefendantRemark3.Text);
                sqlCmd.Parameters.AddWithValue("@PrescriptionPrecludedDate", GetDateTimePickerValue(textBoxPrescriptionPrecludedDate));
                sqlCmd.Parameters.AddWithValue("@DebtRecordDate", GetDateTimePickerValue(textBoxDebtRecordDate));
                sqlCmd.Parameters.AddWithValue("@Capital", textBoxCapital.Text);
                sqlCmd.Parameters.AddWithValue("@Interest", textBoxInterest.Text);
                sqlCmd.Parameters.AddWithValue("@Total", textBoxTotal.Text);
                sqlCmd.Parameters.AddWithValue("@ProsecutorType", comboBoxProsecutorType.Text);
                sqlCmd.Parameters.AddWithValue("@SueStatus", comboBoxSueStatus.Text);
                sqlCmd.Parameters.AddWithValue("@CloseDate", GetDateTimePickerValue(textBoxCloseDate));
                sqlCmd.Parameters.AddWithValue("@Execution", comboBoxExecution.Text);
                sqlCmd.Parameters.AddWithValue("@FileStatus", comboBoxFileStatus.Text);

                SqlParameter outCaseId = new SqlParameter("@CaseId", SqlDbType.Int);
                outCaseId.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(outCaseId);

                database.ExecuteTransactionNonQuery(sqlCmd, sqlTrans);

                string caseId = outCaseId.Value.ToString();

                SqlCommand sqlCmd1 = new SqlCommand("USP_InsertGeneralDetail");
                sqlCmd1.Transaction = sqlTrans;

                sqlCmd1.Parameters.AddWithValue("@CaseId", caseId);
                sqlCmd1.Parameters.AddWithValue("@ComplainantLawyer", comboBoxComplainantLawyer.Text);
                sqlCmd1.Parameters.AddWithValue("@DetectiveLaywer", comboBoxDetectiveLaywer.Text);
                sqlCmd1.Parameters.AddWithValue("@AfterSentenceLawyer", comboBoxAfterSentenceLawyer.Text);
                sqlCmd1.Parameters.AddWithValue("@Disburse1", textBoxDisburse1.Text);
                sqlCmd1.Parameters.AddWithValue("@Disburse2", textBoxDisburse2.Text);
                sqlCmd1.Parameters.AddWithValue("@Disburse3", textBoxDisburse3.Text);
                sqlCmd1.Parameters.AddWithValue("@HelperLawyer", textBoxHelperLawyer.Text);
                sqlCmd1.Parameters.AddWithValue("@DefendantNameList", textBoxDefendantNameList.Text);
                sqlCmd1.Parameters.AddWithValue("@Remark", textBoxRemark.Text);
                sqlCmd1.Parameters.AddWithValue("@YellowResult", textBoxYellowResult.Text);
                sqlCmd1.Parameters.AddWithValue("@ComplaintWorkRel", textBoxComplaintWorkRel.Text);
                sqlCmd1.Parameters.AddWithValue("@Witness", comboBoxWitness.Text);
                sqlCmd1.Parameters.AddWithValue("@Operator", textBoxOperator.Text);
                sqlCmd1.Parameters.AddWithValue("@Mortgage", comboBoxMortgage.Text);
                sqlCmd1.Parameters.AddWithValue("@BSY", comboBoxBSY.Text);
                sqlCmd1.Parameters.AddWithValue("@CaseGroup", comboBoxCaseGroup.Text);
                sqlCmd1.Parameters.AddWithValue("@AssetDetectiveOfficer", comboBoxAssetDetectiveOfficer.Text);
                sqlCmd1.Parameters.AddWithValue("@CaseOfficer", comboBoxCaseOfficer.Text);
                sqlCmd1.Parameters.AddWithValue("@CaseWorkRel", textBoxCaseWorkRel.Text);
                sqlCmd1.Parameters.AddWithValue("@FollowingResult", textBoxFollowingResult.Text);
                sqlCmd1.Parameters.AddWithValue("@CallResult", textBoxCallResult.Text);
                sqlCmd1.Parameters.AddWithValue("@CaseResult", textBoxCaseResult.Text);
                sqlCmd1.Parameters.AddWithValue("@AccountingOfficer", textBoxAccountingOfficer.Text);


                database.ExecuteTransactionNonQuery(sqlCmd1, sqlTrans);

                SqlCommand sqlCmd2 = new SqlCommand("USP_InsertDateDetail");
                sqlCmd2.Transaction = sqlTrans;

                sqlCmd2.Parameters.AddWithValue("@CaseId", caseId);
                sqlCmd2.Parameters.AddWithValue("@AdmissionDate", GetDateTimePickerValue(dateTimePickerAdmissionDate));
                sqlCmd2.Parameters.AddWithValue("@NoticeDate", GetDateTimePickerValue(dateTimePickerNoticeDate));
                sqlCmd2.Parameters.AddWithValue("@FilingDate", GetDateTimePickerValue(dateTimePickerFilingDate));
                sqlCmd2.Parameters.AddWithValue("@DayOfJudgment", GetDateTimePickerValue(dateTimePickerDayOfJudgment));
                sqlCmd2.Parameters.AddWithValue("@MandatoryDate", GetDateTimePickerValue(dateTimePickerMandatoryDate));
                sqlCmd2.Parameters.AddWithValue("@IssuingWarrantDate", GetDateTimePickerValue(dateTimePickerIssuingWarrantDate));
                sqlCmd2.Parameters.AddWithValue("@TerminatingCaseApprovalDate", GetDateTimePickerValue(dateTimePickerTerminatingCaseApprovalDate));
                sqlCmd2.Parameters.AddWithValue("@ApplyingPetitionDate", GetDateTimePickerValue(dateTimePickerApplyingPetitionDate));
                sqlCmd2.Parameters.AddWithValue("@LodgingAppealDate", GetDateTimePickerValue(dateTimePickerLodgingAppealDate));
                sqlCmd2.Parameters.AddWithValue("@CourtFeeRefundRequestDate", GetDateTimePickerValue(dateTimePickerCourtFeeRefundRequestDate));
                sqlCmd2.Parameters.AddWithValue("@ReturningFileDate", GetDateTimePickerValue(dateTimePickerReturningFileDate));
                sqlCmd2.Parameters.AddWithValue("@AdditionDetail", textBoxAdditionDetail.Text);
                sqlCmd2.Parameters.AddWithValue("@ClaimDate", GetDateTimePickerValue(dateTimePickerClaimDate));
                sqlCmd2.Parameters.AddWithValue("@DueDate", GetDateTimePickerValue(dateTimePickerDueDate));
                sqlCmd2.Parameters.AddWithValue("@CourtFee", textBoxCourtFee.Text);
                sqlCmd2.Parameters.AddWithValue("@UnnameDropDown1", comboBoxUnnameDropDown1.Text);
                sqlCmd2.Parameters.AddWithValue("@UnnameDropDown2", comboBoxUnnameDropDown2.Text);
                sqlCmd2.Parameters.AddWithValue("@UnnameCheckBox", checkBoxUnnameCheckBox.Checked);
                sqlCmd2.Parameters.AddWithValue("@JudgementExecutationClosingDate", GetDateTimePickerValue(dateTimePickerJudgementExecutationClosingDate));
                sqlCmd2.Parameters.AddWithValue("@JudgementExecutationDate", GetDateTimePickerValue(dateTimePickJudgementExecutationDate));
                sqlCmd2.Parameters.AddWithValue("@AcceptingAppealJudgementDate", GetDateTimePickerValue(dateTimePickerAcceptingAppealJudgementDate));
                sqlCmd2.Parameters.AddWithValue("@AppealJudgementDate", GetDateTimePickerValue(dateTimePickerAcceptingAppealJudgementDate));
                sqlCmd2.Parameters.AddWithValue("@CourtChequeDate", GetDateTimePickerValue(dateTimePickerCourtChequeDate));
                sqlCmd2.Parameters.AddWithValue("@CourtChequeAmount", textBoxCourtChequeAmount.Text);
                sqlCmd2.Parameters.AddWithValue("@ReturningChequeDate", GetDateTimePickerValue(dateTimePickerReturningChequeDate));
                sqlCmd2.Parameters.AddWithValue("@OpeningCaseDate", GetDateTimePickerValue(dateTimePickerOpeningCaseDate));
                sqlCmd2.Parameters.AddWithValue("@WithdrawalDate", GetDateTimePickerValue(dateTimePickerWithdrawalDate));
                sqlCmd2.Parameters.AddWithValue("@CourtFeeRefundAmount", textBoxCourtFeeRefundAmount.Text);
                sqlCmd2.Parameters.AddWithValue("@ApplyingAttachmentDate", GetDateTimePickerValue(dateTimePickerApplyingAttachmentDate));
                sqlCmd2.Parameters.AddWithValue("@SeizingPropertiesDate", GetDateTimePickerValue(dateTimePickerSeizingPropertiesDate));
                sqlCmd2.Parameters.AddWithValue("@ApplyingDivisionDate", GetDateTimePickerValue(dateTimePickerApplyingDivisionDate));
                sqlCmd2.Parameters.AddWithValue("@SubrogationDate", GetDateTimePickerValue(dateTimePickerSubrogationDate));
                sqlCmd2.Parameters.AddWithValue("@DivisionDate", GetDateTimePickerValue(dateTimePickerDivisionDate));
                sqlCmd2.Parameters.AddWithValue("@RequestForPaymentDate", GetDateTimePickerValue(dateTimePickerRequestForPaymentDate));
                sqlCmd2.Parameters.AddWithValue("@AttachmentAfterSellingDate", GetDateTimePickerValue(dateTimePickerAttachmentAfterSellingDate));
                sqlCmd2.Parameters.AddWithValue("@AbsoluteReceivershipDate", GetDateTimePickerValue(dateTimePickerAbsoluteReceivershipDate));
                sqlCmd2.Parameters.AddWithValue("@SendingProxyDate", GetDateTimePickerValue(dateTimePickerSendingProxyDate));
                sqlCmd2.Parameters.AddWithValue("@DocumentNumber", textBoxDocumentNumber.Text);
                sqlCmd2.Parameters.AddWithValue("@ExecutationAttachment1", GetDateTimePickerValue(dateTimePickerExecutationAttachment1));
                sqlCmd2.Parameters.AddWithValue("@ExecutationAttachment2", GetDateTimePickerValue(dateTimePickerExecutationAttachment2));
                sqlCmd2.Parameters.AddWithValue("@WithdrawalAttachment1", GetDateTimePickerValue(dateTimePickerWithdrawalAttachment1));
                sqlCmd2.Parameters.AddWithValue("@WithdrawalAttachment2", GetDateTimePickerValue(dateTimePickerWithdrawalAttachment2));
                sqlCmd2.Parameters.AddWithValue("@PermittedDate1", GetDateTimePickerValue(dateTimePickerPermittedDate1));
                sqlCmd2.Parameters.AddWithValue("@WithdrawalDate1", GetDateTimePickerValue(dateTimePickerWithdrawalDate1));
                sqlCmd2.Parameters.AddWithValue("@WithdrawalDate2", GetDateTimePickerValue(dateTimePickerWithdrawalDate2));
                sqlCmd2.Parameters.AddWithValue("@PermittedDate2", GetDateTimePickerValue(dateTimePickerPermittedDate2));
                sqlCmd2.Parameters.AddWithValue("@SubrogatedWithdrawalDate", GetDateTimePickerValue(dateTimePickerSubrogatedWithdrawalDate));
                sqlCmd2.Parameters.AddWithValue("@WithdrawalDividedAttachedFundsDate", GetDateTimePickerValue(dateTimePickerWithdrawalDividedAttachedFundsDate));
                sqlCmd2.Parameters.AddWithValue("@PaymentWithdrawalDate1", GetDateTimePickerValue(dateTimePickerPaymentWithdrawalDate1));
                sqlCmd2.Parameters.AddWithValue("@PaymentWithdrawalDate2", GetDateTimePickerValue(dateTimePickerPaymentWithdrawalDate2));
                sqlCmd2.Parameters.AddWithValue("@PaymentWithdrawalDate3", GetDateTimePickerValue(dateTimePickerPaymentWithdrawalDate3));
                sqlCmd2.Parameters.AddWithValue("@WithdrawalOfAttachedFundsAfterSellingDate", GetDateTimePickerValue(dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate));
                sqlCmd2.Parameters.AddWithValue("@AcquiringProxyDate", GetDateTimePickerValue(dateTimePickerAcquiringProxyDate));
                sqlCmd2.Parameters.AddWithValue("@ExpiredCaseDate", GetDateTimePickerValue(dateTimePickerExpiredCaseDate));
                sqlCmd2.Parameters.AddWithValue("@EndOfSellingAuctionDueDate", GetDateTimePickerValue(dateTimePickerEndOfSellingAuctionDueDate));
                sqlCmd2.Parameters.AddWithValue("@UnnameDropDown3", comboBoxUnnameDropDown3.Text);

                database.ExecuteTransactionNonQuery(sqlCmd2, sqlTrans);

                SqlCommand sqlCmd3 = new SqlCommand("USP_InsertJudgementResult");
                sqlCmd3.Transaction = sqlTrans;

                sqlCmd3.Parameters.AddWithValue("@CaseId", caseId);
                sqlCmd3.Parameters.AddWithValue("@Result", textBoxJudgementResult.Text);

                database.ExecuteTransactionNonQuery(sqlCmd3, sqlTrans);


                SqlCommand sqlCmd4 = new SqlCommand("USP_InsertDebtDetail");
                sqlCmd4.Transaction = sqlTrans;

                sqlCmd4.Parameters.AddWithValue("@CaseId", caseId);
                sqlCmd4.Parameters.AddWithValue("@DebtDetail", textBoxDebtDetail.Text);

                database.ExecuteTransactionNonQuery(sqlCmd4, sqlTrans);

                SqlCommand sqlCmd5 = new SqlCommand("USP_InsertLitigateDetail");
                sqlCmd5.Transaction = sqlTrans;

                sqlCmd5.Parameters.AddWithValue("@CaseId", caseId);
                sqlCmd5.Parameters.AddWithValue("@LitigateDetail", textBoxLitigateDetail.Text);

                database.ExecuteTransactionNonQuery(sqlCmd5, sqlTrans);

                SqlCommand sqlCmd6 = new SqlCommand("USP_InsertOperationRegisterDetail");
                sqlCmd6.Transaction = sqlTrans;

                sqlCmd6.Parameters.AddWithValue("@CaseId", caseId);
                sqlCmd6.Parameters.AddWithValue("@OperationRegisterDetail", textBoxOperationRegister.Text);

                database.ExecuteTransactionNonQuery(sqlCmd6, sqlTrans);

                sqlTrans.Commit();
                database.GetSqlConnection().Close();

                System.Windows.Forms.MessageBox.Show("เพิ่มสำเร็จ", "สำเร็จ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_SearchCase");

            sqlCmd.Parameters.AddWithValue("@FileNo1", textBoxSearchFileNo1.Text);
            sqlCmd.Parameters.AddWithValue("@FileNo2", textBoxSearchFileNo2.Text);

            DataTable dt = database.GetDataTable(sqlCmd);

            dataGridViewCase.DataSource = dt;
        }

        private void dataGridViewCase_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == dataGridViewCase.Columns["View"].Index && e.RowIndex > -1)
            {
                SqlCommand sqlCmd = new SqlCommand("USP_GetCase");

                sqlCmd.Parameters.AddWithValue("@CaseId", dataGridViewCase.Rows[e.RowIndex].Cells["CaseId"].Value.ToString());

                DataTable dt = database.GetDataTable(sqlCmd);

                if(dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    this.caseId = dr["CaseId"].ToString();
                    textBoxFileNo1.Text = dr["FileNo1"].ToString();
                    textBoxCompletedFileNo.Text = dr["CompletedFileNo"].ToString();
                    textBoxFileNo2.Text = dr["FileNo2"].ToString();
                    //textBoxCaseNo1.Text = dr["CaseNo1"].ToString();
                    //textBoxCaseNo2.Text = dr["CaseNo2"].ToString();
                    textBoxAccountNo.Text = dr["AccountNo"].ToString();
                    textBoxBlackCaseNo.Text = dr["BlackCaseNo"].ToString();
                    textBoxRedCaseNo.Text = dr["RedCaseNo"].ToString();
                    comboBoxCourtName.Text = dr["CourtName"].ToString();
                    comboBoxCaseType.Text = dr["CaseType"].ToString();
                    comboBoxAllegationType.Text = dr["AllegationType"].ToString();
                    comboBoxProsecutor.Text = dr["Prosecutor"].ToString();
                    comboBoxComplainant.Text = dr["Complainant"].ToString();
                    comboBoxDefendant.Text = dr["Defendant"].ToString();
                    textBoxDefendantRemark1.Text = dr["DefendantRemark1"].ToString();
                    textBoxDefendantRemark2.Text = dr["DefendantRemark2"].ToString();
                    textBoxDefendantRemark3.Text = dr["DefendantRemark3"].ToString();
                    textBoxPrescriptionPrecludedDate = ShowDateTimePickerValue(textBoxPrescriptionPrecludedDate, dr["PrescriptionPrecludedDate"]);
                    textBoxDebtRecordDate = ShowDateTimePickerValue(textBoxDebtRecordDate, dr["DebtRecordDate"]);
                    textBoxCapital.Text = dr["Capital"].ToString();
                    textBoxInterest.Text = dr["Interest"].ToString();
                    textBoxTotal.Text = dr["Total"].ToString();
                    comboBoxProsecutorType.Text = dr["ProsecutorType"].ToString();
                    comboBoxSueStatus.Text = dr["SueStatus"].ToString();
                    textBoxCloseDate = ShowDateTimePickerValue(textBoxCloseDate, dr["CloseDate"]);
                    comboBoxExecution.Text = dr["Execution"].ToString();
                    comboBoxFileStatus.Text = dr["FileStatus"].ToString();

                    comboBoxComplainantLawyer.Text = dr["ComplainantLawyer"].ToString();
                    comboBoxDetectiveLaywer.Text = dr["DetectiveLaywer"].ToString();
                    comboBoxAfterSentenceLawyer.Text = dr["AfterSentenceLawyer"].ToString();
                    textBoxDisburse1.Text = dr["Disburse1"].ToString();
                    textBoxDisburse2.Text = dr["Disburse2"].ToString();
                    textBoxDisburse3.Text = dr["Disburse3"].ToString();
                    textBoxHelperLawyer.Text = dr["HelperLawyer"].ToString();
                    textBoxDefendantNameList.Text = dr["DefendantNameList"].ToString();
                    textBoxRemark.Text = dr["Remark"].ToString();
                    textBoxYellowResult.Text = dr["YellowResult"].ToString();
                    textBoxComplaintWorkRel.Text = dr["ComplaintWorkRel"].ToString();
                    comboBoxWitness.Text = dr["Witness"].ToString();
                    textBoxOperator.Text = dr["Operator"].ToString();
                    comboBoxMortgage.Text = dr["Mortgage"].ToString();
                    comboBoxBSY.Text = dr["BSY"].ToString();
                    comboBoxCaseGroup.Text = dr["CaseGroup"].ToString();
                    comboBoxAssetDetectiveOfficer.Text = dr["AssetDetectiveOfficer"].ToString();
                    comboBoxCaseOfficer.Text = dr["CaseOfficer"].ToString();
                    textBoxCaseWorkRel.Text = dr["CaseWorkRel"].ToString();
                    textBoxFollowingResult.Text = dr["FollowingResult"].ToString();
                    textBoxCallResult.Text = dr["CallResult"].ToString();
                    textBoxCaseResult.Text = dr["CaseResult"].ToString();
                    textBoxAccountingOfficer.Text = dr["AccountingOfficer"].ToString();

                    dateTimePickerAdmissionDate = ShowDateTimePickerValue(dateTimePickerAdmissionDate, dr["AdmissionDate"]);
                    dateTimePickerNoticeDate = ShowDateTimePickerValue(dateTimePickerNoticeDate,dr["NoticeDate"]);
                    dateTimePickerFilingDate = ShowDateTimePickerValue(dateTimePickerFilingDate,dr["FilingDate"]);
                    dateTimePickerDayOfJudgment = ShowDateTimePickerValue(dateTimePickerDayOfJudgment,dr["DayOfJudgment"]);
                    dateTimePickerMandatoryDate = ShowDateTimePickerValue(dateTimePickerMandatoryDate,dr["MandatoryDate"]);
                    dateTimePickerIssuingWarrantDate = ShowDateTimePickerValue(dateTimePickerIssuingWarrantDate,dr["IssuingWarrantDate"]);
                    dateTimePickerTerminatingCaseApprovalDate = ShowDateTimePickerValue(dateTimePickerTerminatingCaseApprovalDate,dr["TerminatingCaseApprovalDate"]);
                    dateTimePickerApplyingPetitionDate = ShowDateTimePickerValue(dateTimePickerApplyingPetitionDate,dr["ApplyingPetitionDate"]);
                    dateTimePickerLodgingAppealDate = ShowDateTimePickerValue(dateTimePickerLodgingAppealDate,dr["LodgingAppealDate"]);
                    dateTimePickerCourtFeeRefundRequestDate = ShowDateTimePickerValue(dateTimePickerCourtFeeRefundRequestDate,dr["CourtFeeRefundRequestDate"]);
                    dateTimePickerReturningFileDate = ShowDateTimePickerValue(dateTimePickerReturningFileDate,dr["ReturningFileDate"]);
                    textBoxAdditionDetail.Text = dr["AdditionDetail"].ToString();
                    dateTimePickerClaimDate = ShowDateTimePickerValue(dateTimePickerClaimDate,dr["ClaimDate"]);
                    dateTimePickerDueDate = ShowDateTimePickerValue(dateTimePickerDueDate,dr["DueDate"]);
                    textBoxCourtFee.Text = dr["CourtFee"].ToString();
                    comboBoxUnnameDropDown1.Text = dr["UnnameDropDown1"].ToString();
                    comboBoxUnnameDropDown2.Text = dr["UnnameDropDown2"].ToString();
                    checkBoxUnnameCheckBox.Checked = Convert.ToBoolean(dr["UnnameCheckBox"]);
                    dateTimePickerJudgementExecutationClosingDate = ShowDateTimePickerValue(dateTimePickerJudgementExecutationClosingDate,dr["JudgementExecutationClosingDate"]);
                    dateTimePickJudgementExecutationDate = ShowDateTimePickerValue(dateTimePickJudgementExecutationDate,dr["JudgementExecutationDate"]);
                    dateTimePickerAcceptingAppealJudgementDate = ShowDateTimePickerValue(dateTimePickerAcceptingAppealJudgementDate,dr["AcceptingAppealJudgementDate"]);
                    dateTimePickerAcceptingAppealJudgementDate = ShowDateTimePickerValue(dateTimePickerAcceptingAppealJudgementDate,dr["AcceptingAppealJudgementDate"]);
                    dateTimePickerCourtChequeDate = ShowDateTimePickerValue(dateTimePickerCourtChequeDate,dr["CourtChequeDate"]);
                    textBoxCourtChequeAmount.Text = dr["CourtChequeAmount"].ToString();
                    dateTimePickerReturningChequeDate = ShowDateTimePickerValue(dateTimePickerReturningChequeDate,dr["ReturningChequeDate"]);
                    dateTimePickerOpeningCaseDate = ShowDateTimePickerValue(dateTimePickerOpeningCaseDate,dr["OpeningCaseDate"]);
                    dateTimePickerWithdrawalDate = ShowDateTimePickerValue(dateTimePickerWithdrawalDate,dr["WithdrawalDate"]);
                    textBoxCourtFeeRefundAmount.Text = dr["CourtFeeRefundAmount"].ToString();
                    dateTimePickerApplyingAttachmentDate = ShowDateTimePickerValue(dateTimePickerApplyingAttachmentDate,dr["ApplyingAttachmentDate"]);
                    dateTimePickerSeizingPropertiesDate = ShowDateTimePickerValue(dateTimePickerSeizingPropertiesDate,dr["SeizingPropertiesDate"]);
                    dateTimePickerApplyingDivisionDate = ShowDateTimePickerValue(dateTimePickerApplyingDivisionDate,dr["ApplyingDivisionDate"]);
                    dateTimePickerSubrogationDate = ShowDateTimePickerValue(dateTimePickerSubrogationDate,dr["SubrogationDate"]);
                    dateTimePickerDivisionDate = ShowDateTimePickerValue(dateTimePickerDivisionDate,dr["DivisionDate"]);
                    dateTimePickerRequestForPaymentDate = ShowDateTimePickerValue(dateTimePickerRequestForPaymentDate,dr["RequestForPaymentDate"]);
                    dateTimePickerAttachmentAfterSellingDate = ShowDateTimePickerValue(dateTimePickerAttachmentAfterSellingDate,dr["AttachmentAfterSellingDate"]);
                    dateTimePickerAbsoluteReceivershipDate = ShowDateTimePickerValue(dateTimePickerAbsoluteReceivershipDate,dr["AbsoluteReceivershipDate"]);
                    dateTimePickerSendingProxyDate = ShowDateTimePickerValue(dateTimePickerSendingProxyDate,dr["SendingProxyDate"]);
                    textBoxDocumentNumber.Text = dr["DocumentNumber"].ToString();
                    dateTimePickerExecutationAttachment1 = ShowDateTimePickerValue(dateTimePickerExecutationAttachment1,dr["ExecutationAttachment1"]);
                    dateTimePickerExecutationAttachment2 = ShowDateTimePickerValue(dateTimePickerExecutationAttachment2,dr["ExecutationAttachment2"]);
                    dateTimePickerWithdrawalAttachment1 = ShowDateTimePickerValue(dateTimePickerWithdrawalAttachment1,dr["WithdrawalAttachment1"]);
                    dateTimePickerWithdrawalAttachment2 = ShowDateTimePickerValue(dateTimePickerWithdrawalAttachment2,dr["WithdrawalAttachment2"]);
                    dateTimePickerPermittedDate1 = ShowDateTimePickerValue(dateTimePickerPermittedDate1,dr["PermittedDate1"]);
                    dateTimePickerWithdrawalDate1 = ShowDateTimePickerValue(dateTimePickerWithdrawalDate1,dr["WithdrawalDate1"]);
                    dateTimePickerWithdrawalDate2 = ShowDateTimePickerValue(dateTimePickerWithdrawalDate2,dr["WithdrawalDate2"]);
                    dateTimePickerPermittedDate2 = ShowDateTimePickerValue(dateTimePickerPermittedDate2,dr["PermittedDate2"]);
                    dateTimePickerSubrogatedWithdrawalDate = ShowDateTimePickerValue(dateTimePickerSubrogatedWithdrawalDate,dr["SubrogatedWithdrawalDate"]);
                    dateTimePickerWithdrawalDividedAttachedFundsDate = ShowDateTimePickerValue(dateTimePickerWithdrawalDividedAttachedFundsDate,dr["WithdrawalDividedAttachedFundsDate"]);
                    dateTimePickerPaymentWithdrawalDate1 = ShowDateTimePickerValue(dateTimePickerPaymentWithdrawalDate1,dr["PaymentWithdrawalDate1"]);
                    dateTimePickerPaymentWithdrawalDate2 = ShowDateTimePickerValue(dateTimePickerPaymentWithdrawalDate2,dr["PaymentWithdrawalDate2"]);
                    dateTimePickerPaymentWithdrawalDate3 = ShowDateTimePickerValue(dateTimePickerPaymentWithdrawalDate3,dr["PaymentWithdrawalDate3"]);
                    dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate = ShowDateTimePickerValue(dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate,dr["WithdrawalOfAttachedFundsAfterSellingDate"]);
                    dateTimePickerAcquiringProxyDate = ShowDateTimePickerValue(dateTimePickerAcquiringProxyDate,dr["AcquiringProxyDate"]);
                    dateTimePickerExpiredCaseDate = ShowDateTimePickerValue(dateTimePickerExpiredCaseDate,dr["ExpiredCaseDate"]);
                    dateTimePickerEndOfSellingAuctionDueDate = ShowDateTimePickerValue(dateTimePickerEndOfSellingAuctionDueDate, dr["EndOfSellingAuctionDueDate"]);
                    comboBoxUnnameDropDown3.Text = dr["UnnameDropDown3"].ToString();

                    textBoxJudgementResult.Text = dr["JudgementResult"].ToString();

                    textBoxDebtDetail.Text = dr["DebtDetail"].ToString();

                    textBoxLitigateDetail.Text = dr["LitigateDetail"].ToString();

                    textBoxOperationRegister.Text = dr["OperationRegisterDetail"].ToString();
                }
            }
        }

        private object GetDateTimePickerValue(DateTimePicker dateTimePicker)
        {
            if(dateTimePicker.CustomFormat == " ")
            {
                return DBNull.Value;
            }

            else
            {
                return dateTimePicker.Value;
            }
        }

        private DateTimePicker ShowDateTimePickerValue(DateTimePicker dateTimePicker, object dateTime)
        {
            if (dateTime.ToString() == string.Empty)
            {
                dateTimePicker.CustomFormat = " ";
            }

            else
            {
                dateTimePicker.CustomFormat = "dd/MM/yyyy";
                dateTimePicker.Value = Convert.ToDateTime(dateTime);
            }

            return dateTimePicker;
        }

        private void DateTimePickerValueChanged(object sender, EventArgs e)
        {
           ((DateTimePicker)sender).Format = DateTimePickerFormat.Custom;
           ((DateTimePicker)sender).CustomFormat = "dd/MM/yyyy";
        }

        private void ClearDateTimePickerValue(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete) 
            {
                ((DateTimePicker)sender).CustomFormat = " ";
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SqlTransaction sqlTrans = null;
                database.GetSqlConnection().Open();
            sqlTrans = database.GetSqlConnection().BeginTransaction();

            SqlCommand sqlCmd = new SqlCommand("USP_UpdateCase");
            sqlCmd.Transaction = sqlTrans;

            sqlCmd.Parameters.AddWithValue("@CaseId", this.caseId);
            sqlCmd.Parameters.AddWithValue("@FileNo1", textBoxFileNo1.Text);
            sqlCmd.Parameters.AddWithValue("@CompletedFileNo", textBoxCompletedFileNo.Text);
            sqlCmd.Parameters.AddWithValue("@FileNo2", textBoxFileNo2.Text);
            sqlCmd.Parameters.AddWithValue("@CaseNo1", "");//textBoxCaseNo1.Text);
            sqlCmd.Parameters.AddWithValue("@CaseNo2", "");//textBoxCaseNo2.Text);
            sqlCmd.Parameters.AddWithValue("@AccountNo", textBoxAccountNo.Text);
            sqlCmd.Parameters.AddWithValue("@BlackCaseNo", textBoxBlackCaseNo.Text);
            sqlCmd.Parameters.AddWithValue("@RedCaseNo", textBoxRedCaseNo.Text);
            sqlCmd.Parameters.AddWithValue("@CourtName", comboBoxCourtName.Text);
            sqlCmd.Parameters.AddWithValue("@CaseType", comboBoxCaseType.Text);
            sqlCmd.Parameters.AddWithValue("@AllegationType", comboBoxAllegationType.Text);
            sqlCmd.Parameters.AddWithValue("@Prosecutor", comboBoxProsecutor.Text);
            sqlCmd.Parameters.AddWithValue("@Complainant", comboBoxComplainant.Text);
            sqlCmd.Parameters.AddWithValue("@Defendant", comboBoxDefendant.Text);
            sqlCmd.Parameters.AddWithValue("@DefendantRemark1", textBoxDefendantRemark1.Text);
            sqlCmd.Parameters.AddWithValue("@DefendantRemark2", textBoxDefendantRemark2.Text);
            sqlCmd.Parameters.AddWithValue("@DefendantRemark3", textBoxDefendantRemark3.Text);
            sqlCmd.Parameters.AddWithValue("@PrescriptionPrecludedDate", GetDateTimePickerValue(textBoxPrescriptionPrecludedDate));
            sqlCmd.Parameters.AddWithValue("@DebtRecordDate", GetDateTimePickerValue(textBoxDebtRecordDate));
            sqlCmd.Parameters.AddWithValue("@Capital", textBoxCapital.Text);
            sqlCmd.Parameters.AddWithValue("@Interest", textBoxInterest.Text);
            sqlCmd.Parameters.AddWithValue("@Total", textBoxTotal.Text);
            sqlCmd.Parameters.AddWithValue("@ProsecutorType", comboBoxProsecutorType.Text);
            sqlCmd.Parameters.AddWithValue("@SueStatus", comboBoxSueStatus.Text);
            sqlCmd.Parameters.AddWithValue("@CloseDate", GetDateTimePickerValue(textBoxCloseDate));
            sqlCmd.Parameters.AddWithValue("@Execution", comboBoxExecution.Text);
            sqlCmd.Parameters.AddWithValue("@FileStatus", comboBoxFileStatus.Text);

            database.ExecuteTransactionNonQuery(sqlCmd, sqlTrans);

            SqlCommand sqlCmd1 = new SqlCommand("USP_UpdateGeneralDetail");
            sqlCmd1.Transaction = sqlTrans;

            sqlCmd1.Parameters.AddWithValue("@CaseId", this.caseId);
            sqlCmd1.Parameters.AddWithValue("@ComplainantLawyer", comboBoxComplainantLawyer.Text);
            sqlCmd1.Parameters.AddWithValue("@DetectiveLaywer", comboBoxDetectiveLaywer.Text);
            sqlCmd1.Parameters.AddWithValue("@AfterSentenceLawyer", comboBoxAfterSentenceLawyer.Text);
            sqlCmd1.Parameters.AddWithValue("@Disburse1", textBoxDisburse1.Text);
            sqlCmd1.Parameters.AddWithValue("@Disburse2", textBoxDisburse2.Text);
            sqlCmd1.Parameters.AddWithValue("@Disburse3", textBoxDisburse3.Text);
            sqlCmd1.Parameters.AddWithValue("@HelperLawyer", textBoxHelperLawyer.Text);
            sqlCmd1.Parameters.AddWithValue("@DefendantNameList", textBoxDefendantNameList.Text);
            sqlCmd1.Parameters.AddWithValue("@Remark", textBoxRemark.Text);
            sqlCmd1.Parameters.AddWithValue("@YellowResult", textBoxYellowResult.Text);
            sqlCmd1.Parameters.AddWithValue("@ComplaintWorkRel", textBoxComplaintWorkRel.Text);
            sqlCmd1.Parameters.AddWithValue("@Witness", comboBoxWitness.Text);
            sqlCmd1.Parameters.AddWithValue("@Operator", textBoxOperator.Text);
            sqlCmd1.Parameters.AddWithValue("@Mortgage", comboBoxMortgage.Text);
            sqlCmd1.Parameters.AddWithValue("@BSY", comboBoxBSY.Text);
            sqlCmd1.Parameters.AddWithValue("@CaseGroup", comboBoxCaseGroup.Text);
            sqlCmd1.Parameters.AddWithValue("@AssetDetectiveOfficer", comboBoxAssetDetectiveOfficer.Text);
            sqlCmd1.Parameters.AddWithValue("@CaseOfficer", comboBoxCaseOfficer.Text);
            sqlCmd1.Parameters.AddWithValue("@CaseWorkRel", textBoxCaseWorkRel.Text);
            sqlCmd1.Parameters.AddWithValue("@FollowingResult", textBoxFollowingResult.Text);
            sqlCmd1.Parameters.AddWithValue("@CallResult", textBoxCallResult.Text);
            sqlCmd1.Parameters.AddWithValue("@CaseResult", textBoxCaseResult.Text);
            sqlCmd1.Parameters.AddWithValue("@AccountingOfficer", textBoxAccountingOfficer.Text);


            database.ExecuteTransactionNonQuery(sqlCmd1, sqlTrans);
            
            SqlCommand sqlCmd2 = new SqlCommand("USP_UpdateDateDetail");
            sqlCmd2.Transaction = sqlTrans;

            sqlCmd2.Parameters.AddWithValue("@CaseId", this.caseId);
            sqlCmd2.Parameters.AddWithValue("@AdmissionDate", GetDateTimePickerValue(dateTimePickerAdmissionDate));
            sqlCmd2.Parameters.AddWithValue("@NoticeDate", GetDateTimePickerValue(dateTimePickerNoticeDate));
            sqlCmd2.Parameters.AddWithValue("@FilingDate", GetDateTimePickerValue(dateTimePickerFilingDate));
            sqlCmd2.Parameters.AddWithValue("@DayOfJudgment", GetDateTimePickerValue(dateTimePickerDayOfJudgment));
            sqlCmd2.Parameters.AddWithValue("@MandatoryDate", GetDateTimePickerValue(dateTimePickerMandatoryDate));
            sqlCmd2.Parameters.AddWithValue("@IssuingWarrantDate", GetDateTimePickerValue(dateTimePickerIssuingWarrantDate));
            sqlCmd2.Parameters.AddWithValue("@TerminatingCaseApprovalDate", GetDateTimePickerValue(dateTimePickerTerminatingCaseApprovalDate));
            sqlCmd2.Parameters.AddWithValue("@ApplyingPetitionDate", GetDateTimePickerValue(dateTimePickerApplyingPetitionDate));
            sqlCmd2.Parameters.AddWithValue("@LodgingAppealDate", GetDateTimePickerValue(dateTimePickerLodgingAppealDate));
            sqlCmd2.Parameters.AddWithValue("@CourtFeeRefundRequestDate", GetDateTimePickerValue(dateTimePickerCourtFeeRefundRequestDate));
            sqlCmd2.Parameters.AddWithValue("@ReturningFileDate", GetDateTimePickerValue(dateTimePickerReturningFileDate));
            sqlCmd2.Parameters.AddWithValue("@AdditionDetail", textBoxAdditionDetail.Text);
            sqlCmd2.Parameters.AddWithValue("@ClaimDate", GetDateTimePickerValue(dateTimePickerClaimDate));
            sqlCmd2.Parameters.AddWithValue("@DueDate", GetDateTimePickerValue(dateTimePickerDueDate));
            sqlCmd2.Parameters.AddWithValue("@CourtFee", textBoxCourtFee.Text);
            sqlCmd2.Parameters.AddWithValue("@UnnameDropDown1", comboBoxUnnameDropDown1.Text);
            sqlCmd2.Parameters.AddWithValue("@UnnameDropDown2", comboBoxUnnameDropDown2.Text);
            sqlCmd2.Parameters.AddWithValue("@UnnameCheckBox", checkBoxUnnameCheckBox.Checked);
            sqlCmd2.Parameters.AddWithValue("@JudgementExecutationClosingDate", GetDateTimePickerValue(dateTimePickerJudgementExecutationClosingDate));
            sqlCmd2.Parameters.AddWithValue("@JudgementExecutationDate", GetDateTimePickerValue(dateTimePickJudgementExecutationDate));
            sqlCmd2.Parameters.AddWithValue("@AcceptingAppealJudgementDate", GetDateTimePickerValue(dateTimePickerAcceptingAppealJudgementDate));
            sqlCmd2.Parameters.AddWithValue("@AppealJudgementDate", GetDateTimePickerValue(dateTimePickerAcceptingAppealJudgementDate));
            sqlCmd2.Parameters.AddWithValue("@CourtChequeDate", GetDateTimePickerValue(dateTimePickerCourtChequeDate));
            sqlCmd2.Parameters.AddWithValue("@CourtChequeAmount", textBoxCourtChequeAmount.Text);
            sqlCmd2.Parameters.AddWithValue("@ReturningChequeDate", GetDateTimePickerValue(dateTimePickerReturningChequeDate));
            sqlCmd2.Parameters.AddWithValue("@OpeningCaseDate", GetDateTimePickerValue(dateTimePickerOpeningCaseDate));
            sqlCmd2.Parameters.AddWithValue("@WithdrawalDate", GetDateTimePickerValue(dateTimePickerWithdrawalDate));
            sqlCmd2.Parameters.AddWithValue("@CourtFeeRefundAmount", textBoxCourtFeeRefundAmount.Text);
            sqlCmd2.Parameters.AddWithValue("@ApplyingAttachmentDate", GetDateTimePickerValue(dateTimePickerApplyingAttachmentDate));
            sqlCmd2.Parameters.AddWithValue("@SeizingPropertiesDate", GetDateTimePickerValue(dateTimePickerSeizingPropertiesDate));
            sqlCmd2.Parameters.AddWithValue("@ApplyingDivisionDate", GetDateTimePickerValue(dateTimePickerApplyingDivisionDate));
            sqlCmd2.Parameters.AddWithValue("@SubrogationDate", GetDateTimePickerValue(dateTimePickerSubrogationDate));
            sqlCmd2.Parameters.AddWithValue("@DivisionDate", GetDateTimePickerValue(dateTimePickerDivisionDate));
            sqlCmd2.Parameters.AddWithValue("@RequestForPaymentDate", GetDateTimePickerValue(dateTimePickerRequestForPaymentDate));
            sqlCmd2.Parameters.AddWithValue("@AttachmentAfterSellingDate", GetDateTimePickerValue(dateTimePickerAttachmentAfterSellingDate));
            sqlCmd2.Parameters.AddWithValue("@AbsoluteReceivershipDate", GetDateTimePickerValue(dateTimePickerAbsoluteReceivershipDate));
            sqlCmd2.Parameters.AddWithValue("@SendingProxyDate", GetDateTimePickerValue(dateTimePickerSendingProxyDate));
            sqlCmd2.Parameters.AddWithValue("@DocumentNumber", textBoxDocumentNumber.Text);
            sqlCmd2.Parameters.AddWithValue("@ExecutationAttachment1", GetDateTimePickerValue(dateTimePickerExecutationAttachment1));
            sqlCmd2.Parameters.AddWithValue("@ExecutationAttachment2", GetDateTimePickerValue(dateTimePickerExecutationAttachment2));
            sqlCmd2.Parameters.AddWithValue("@WithdrawalAttachment1", GetDateTimePickerValue(dateTimePickerWithdrawalAttachment1));
            sqlCmd2.Parameters.AddWithValue("@WithdrawalAttachment2", GetDateTimePickerValue(dateTimePickerWithdrawalAttachment2));
            sqlCmd2.Parameters.AddWithValue("@PermittedDate1", GetDateTimePickerValue(dateTimePickerPermittedDate1));
            sqlCmd2.Parameters.AddWithValue("@WithdrawalDate1", GetDateTimePickerValue(dateTimePickerWithdrawalDate1));
            sqlCmd2.Parameters.AddWithValue("@WithdrawalDate2", GetDateTimePickerValue(dateTimePickerWithdrawalDate2));
            sqlCmd2.Parameters.AddWithValue("@PermittedDate2", GetDateTimePickerValue(dateTimePickerPermittedDate2));
            sqlCmd2.Parameters.AddWithValue("@SubrogatedWithdrawalDate", GetDateTimePickerValue(dateTimePickerSubrogatedWithdrawalDate));
            sqlCmd2.Parameters.AddWithValue("@WithdrawalDividedAttachedFundsDate", GetDateTimePickerValue(dateTimePickerWithdrawalDividedAttachedFundsDate));
            sqlCmd2.Parameters.AddWithValue("@PaymentWithdrawalDate1", GetDateTimePickerValue(dateTimePickerPaymentWithdrawalDate1));
            sqlCmd2.Parameters.AddWithValue("@PaymentWithdrawalDate2", GetDateTimePickerValue(dateTimePickerPaymentWithdrawalDate2));
            sqlCmd2.Parameters.AddWithValue("@PaymentWithdrawalDate3", GetDateTimePickerValue(dateTimePickerPaymentWithdrawalDate3));
            sqlCmd2.Parameters.AddWithValue("@WithdrawalOfAttachedFundsAfterSellingDate", GetDateTimePickerValue(dateTimePickerWithdrawalOfAttachedFundsAfterSellingDate));
            sqlCmd2.Parameters.AddWithValue("@AcquiringProxyDate", GetDateTimePickerValue(dateTimePickerAcquiringProxyDate));
            sqlCmd2.Parameters.AddWithValue("@ExpiredCaseDate", GetDateTimePickerValue(dateTimePickerExpiredCaseDate));
            sqlCmd2.Parameters.AddWithValue("@EndOfSellingAuctionDueDate", GetDateTimePickerValue(dateTimePickerEndOfSellingAuctionDueDate));
            sqlCmd2.Parameters.AddWithValue("@UnnameDropDown3", comboBoxUnnameDropDown3.Text);

            database.ExecuteTransactionNonQuery(sqlCmd2, sqlTrans);

            SqlCommand sqlCmd3 = new SqlCommand("USP_UpdateJudgementResult");
            sqlCmd3.Transaction = sqlTrans;

            sqlCmd3.Parameters.AddWithValue("@CaseId", caseId);
            sqlCmd3.Parameters.AddWithValue("@Result", textBoxJudgementResult.Text);

            database.ExecuteTransactionNonQuery(sqlCmd3, sqlTrans);

            SqlCommand sqlCmd4 = new SqlCommand("USP_UpdateDebtDetail");
            sqlCmd4.Transaction = sqlTrans;

            sqlCmd4.Parameters.AddWithValue("@CaseId", caseId);
            sqlCmd4.Parameters.AddWithValue("@DebtDetail", textBoxDebtDetail.Text);

            database.ExecuteTransactionNonQuery(sqlCmd4, sqlTrans);

            SqlCommand sqlCmd5 = new SqlCommand("USP_UpdateLitigateDetail");
            sqlCmd5.Transaction = sqlTrans;

            sqlCmd5.Parameters.AddWithValue("@CaseId", caseId);
            sqlCmd5.Parameters.AddWithValue("@LitigateDetail", textBoxLitigateDetail.Text);

            database.ExecuteTransactionNonQuery(sqlCmd5, sqlTrans);

            SqlCommand sqlCmd6 = new SqlCommand("USP_UpdateOperationRegisterDetail");
            sqlCmd6.Transaction = sqlTrans;

            sqlCmd6.Parameters.AddWithValue("@CaseId", caseId);
            sqlCmd6.Parameters.AddWithValue("@OperationRegisterDetail", textBoxOperationRegister.Text);

            database.ExecuteTransactionNonQuery(sqlCmd6, sqlTrans);

            sqlTrans.Commit();
            database.GetSqlConnection().Close();

            System.Windows.Forms.MessageBox.Show("อัพเดทสำเร็จ", "สำเร็จ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void menuCourt_Click(object sender, EventArgs e)
        {
            Court court = new Court();
            court.ShowDialog();
        }

        private void menuItemAllegationType_Click(object sender, EventArgs e)
        {
            formAllegationType allegationType = new formAllegationType();
            allegationType.ShowDialog();
        }

        private void menuItemProsecutor_Click(object sender, EventArgs e)
        {
            Prosecutor prosecutor = new Prosecutor();
            prosecutor.ShowDialog();
        }

        private void menuItemComplainant_Click(object sender, EventArgs e)
        {
            Complainant complainant = new Complainant();
            complainant.ShowDialog();
        }

        private void menuItemDefendant_Click(object sender, EventArgs e)
        {
            Defendant defendant = new Defendant();
            defendant.ShowDialog();
        }

        private void menuItemLawyer_Click(object sender, EventArgs e)
        {
            Lawyer lawyer = new Lawyer();
            lawyer.ShowDialog();
        }

        private void menuItemWorkDetail_Click(object sender, EventArgs e)
        {
            AlertWorkDetail alertWorkDetail = new AlertWorkDetail();
            alertWorkDetail.ShowDialog();
        }
    }
}
