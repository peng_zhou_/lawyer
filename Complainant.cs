﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lawyer
{
    public partial class Complainant : Form
    {
        Database database = new Database();
        string complainantId = "0";

        public Complainant()
        {
            InitializeComponent();

            dataGridViewComplainant.AutoGenerateColumns = false;
            BindComplainantData();
        }

        private void BindComplainantData()
        {
            SqlCommand sqlCmd = new SqlCommand("USP_GetComplainantData");
            dataGridViewComplainant.DataSource = database.GetDataTable(sqlCmd);
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_InsertComplainant");
            sqlCmd.Parameters.AddWithValue("@ComplainantName", textBoxComplainantName.Text);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกเพิ่มแล้ว.");

                BindComplainantData();
            }
        }

        private void ClearForm()
        {
            this.complainantId = "0";
            textBoxComplainantName.Text = "";

            btnAdd.Enabled = true;
            btnModify.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_UpdateComplainant");
            sqlCmd.Parameters.AddWithValue("@ComplainantId", this.complainantId);
            sqlCmd.Parameters.AddWithValue("@ComplainantName", textBoxComplainantName.Text);

            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกแก้ไขแล้ว.");

                ClearForm();

                BindComplainantData();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_DeleteComplainant");
            sqlCmd.Parameters.AddWithValue("@ComplainantId", this.complainantId);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {

                MessageBox.Show("ข้อมูลถูกลบแล้ว.");

                ClearForm();

                BindComplainantData();
            }
        }

        private void dataGridViewComplainant_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewComplainant.Columns[e.ColumnIndex].Name == "Select")
            {
                this.complainantId = dataGridViewComplainant.Rows[e.RowIndex].Cells["Id"].Value.ToString();

                textBoxComplainantName.Text = dataGridViewComplainant.Rows[e.RowIndex].Cells["ComplainantName"].Value.ToString();

                btnAdd.Enabled = false;
                btnModify.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void dataGridViewComplainant_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dataGridViewComplainant.Rows[e.RowIndex].Cells["RecordNo"].Value = e.RowIndex + 1;
            }
        }
    }
}
