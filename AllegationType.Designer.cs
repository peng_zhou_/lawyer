﻿namespace Lawyer
{
    partial class formAllegationType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewAllegationType = new System.Windows.Forms.DataGridView();
            this.textBoxAllegationType = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Select = new System.Windows.Forms.DataGridViewLinkColumn();
            this.AllegationTypeId1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AllegationType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllegationType)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.ForeColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(284, 56);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 34;
            this.btnDelete.Text = "ลบ";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.Enabled = false;
            this.btnModify.ForeColor = System.Drawing.Color.Blue;
            this.btnModify.Location = new System.Drawing.Point(202, 56);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 32;
            this.btnModify.Text = "แก้ไข";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAdd.Location = new System.Drawing.Point(121, 56);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 31;
            this.btnAdd.Text = "เพิ่ม";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gold;
            this.panel1.Location = new System.Drawing.Point(-131, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 48);
            this.panel1.TabIndex = 35;
            // 
            // dataGridViewAllegationType
            // 
            this.dataGridViewAllegationType.AllowUserToAddRows = false;
            this.dataGridViewAllegationType.AllowUserToDeleteRows = false;
            this.dataGridViewAllegationType.AllowUserToResizeRows = false;
            this.dataGridViewAllegationType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAllegationType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Select,
            this.AllegationTypeId1,
            this.RecordNo,
            this.AllegationType});
            this.dataGridViewAllegationType.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewAllegationType.Location = new System.Drawing.Point(38, 98);
            this.dataGridViewAllegationType.Name = "dataGridViewAllegationType";
            this.dataGridViewAllegationType.Size = new System.Drawing.Size(509, 150);
            this.dataGridViewAllegationType.TabIndex = 30;
            this.dataGridViewAllegationType.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAllegationType_CellContentClick);
            this.dataGridViewAllegationType.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewAllegationType_CellFormatting);
            // 
            // textBoxAllegationType
            // 
            this.textBoxAllegationType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxAllegationType.Location = new System.Drawing.Point(96, 12);
            this.textBoxAllegationType.Name = "textBoxAllegationType";
            this.textBoxAllegationType.Size = new System.Drawing.Size(460, 22);
            this.textBoxAllegationType.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(51, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 16);
            this.label4.TabIndex = 26;
            this.label4.Text = "ข้อหา";
            // 
            // Select
            // 
            this.Select.HeaderText = "เลือก";
            this.Select.Name = "Select";
            this.Select.Text = "เลือก";
            this.Select.ToolTipText = "เลือก";
            this.Select.UseColumnTextForLinkValue = true;
            // 
            // AllegationTypeId1
            // 
            this.AllegationTypeId1.DataPropertyName = "AllegationTypeId";
            this.AllegationTypeId1.HeaderText = "AllegationTypeId";
            this.AllegationTypeId1.Name = "AllegationTypeId1";
            this.AllegationTypeId1.Visible = false;
            // 
            // RecordNo
            // 
            this.RecordNo.HeaderText = "No.";
            this.RecordNo.Name = "RecordNo";
            // 
            // AllegationType
            // 
            this.AllegationType.DataPropertyName = "AllegationType";
            this.AllegationType.HeaderText = "ข้อหา";
            this.AllegationType.Name = "AllegationType";
            // 
            // formAllegationType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 445);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridViewAllegationType);
            this.Controls.Add(this.textBoxAllegationType);
            this.Controls.Add(this.label4);
            this.Name = "formAllegationType";
            this.Text = "ข้อหา";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllegationType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridViewAllegationType;
        private System.Windows.Forms.TextBox textBoxAllegationType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewLinkColumn Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn AllegationTypeId1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AllegationType;

    }
}