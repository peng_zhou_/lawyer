﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lawyer
{
    class MyListControl
    {
        public static void BindComboBox(ComboBox comboBox, DataTable dt)
        {
            comboBox.DataSource = dt;
            comboBox.ValueMember = dt.Columns[0].ColumnName;
            comboBox.DisplayMember = dt.Columns[1].ColumnName;
        }

        public static void BindComboBox(ComboBox comboBox, DataTable dt, string firstItem)
        {
            Dictionary<string, string> data1 = new Dictionary<string, string>();
            data1.Add("0", firstItem);

            foreach(DataRow dr in dt.Rows)
            {
                data1.Add(dr[0].ToString(), dr[1].ToString());

            }

            if (dt.Rows.Count > 0)
            {
            comboBox.DataSource = data1.ToList();

                comboBox.DisplayMember = "value";
                comboBox.ValueMember = "key";
            }
            //comboBox.DataSource = dt;

            //comboBox.ValueMember = dt.Columns[0].ColumnName;
            //comboBox.DisplayMember = dt.Columns[1].ColumnName;
            //comboBox.Items.Insert(0, "")
        }

        internal static void BindComboBox(CheckedListBox checkedListBox, DataTable dt, string firstItem)
        {
            Dictionary<string, string> data1 = new Dictionary<string, string>();
            //data1.Add("0", "-Select-");

            foreach (DataRow dr in dt.Rows)
            {
                data1.Add(dr[0].ToString(), dr[1].ToString());

            }

            if (dt.Rows.Count > 0)
            {
                checkedListBox.DataSource = data1.ToList();

                checkedListBox.DisplayMember = "value";
                checkedListBox.ValueMember = "key";
            }

            if (dt.Rows.Count == 0)
            {
                checkedListBox.DataSource = null;
            }
            //comboBox.DataSource = dt;

            //comboBox.ValueMember = dt.Columns[0].ColumnName;
            //comboBox.DisplayMember = dt.Columns[1].ColumnName;
            //comboBox.Items.Insert(0, "")
        }
    }
}
