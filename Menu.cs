﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lawyer
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void menuCourt_Click(object sender, EventArgs e)
        {
            Court court = new Court();
            court.ShowDialog();
        }

        private void menuItemAllegationType_Click(object sender, EventArgs e)
        {
            formAllegationType allegationType = new formAllegationType();
            allegationType.ShowDialog();
        }

        private void menuItemProsecutor_Click(object sender, EventArgs e)
        {
            Prosecutor prosecutor = new Prosecutor();
            prosecutor.ShowDialog();
        }

        private void menuItemComplainant_Click(object sender, EventArgs e)
        {
            Complainant complainant = new Complainant();
            complainant.ShowDialog();
        }

        private void menuItemDefendant_Click(object sender, EventArgs e)
        {
            Defendant defendant = new Defendant();
            defendant.ShowDialog();
        }

        private void menuItemLawyer_Click(object sender, EventArgs e)
        {
            Lawyer lawyer = new Lawyer();
            lawyer.ShowDialog();
        }

        private void menuItemWorkDetail_Click(object sender, EventArgs e)
        {
            AlertWorkDetail alertWorkDetail = new AlertWorkDetail();
            alertWorkDetail.ShowDialog();
        }

        private void menuItemComplaninantWork_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.ShowDialog();
        }

        private void menuItemCaseWork_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();
        }
    }
}
