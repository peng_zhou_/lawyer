﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lawyer
{
    public partial class Lawyer : Form
    {
        Database database = new Database();
        string lawyerId = "0";

        public Lawyer()
        {
            InitializeComponent();

            comboBoxLawyerType.SelectedIndex = 0;
        }

        private void BindLawyerData()
        {
            SqlCommand sqlCmd = new SqlCommand("USP_GetLawyerData");
            dataGridViewLawyer.DataSource = database.GetDataTable(sqlCmd);

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_InsertLawyer");
            sqlCmd.Parameters.AddWithValue("@LawyerName", textBoxLawyerName.Text);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกเพิ่มแล้ว.");

                BindLawyerData();
            }
        }

        private void ClearForm()
        {
            this.lawyerId = "0";
            textBoxLawyerName.Text = "";

            btnAdd.Enabled = true;
            btnModify.Enabled = false;
            btnDelete.Enabled = false;

        }
        private void btnModify_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_UpdateLawyer");
            sqlCmd.Parameters.AddWithValue("@LawyerId", this.lawyerId);
            sqlCmd.Parameters.AddWithValue("@LawyerName", textBoxLawyerName.Text);

            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกแก้ไขแล้ว.");

                ClearForm();

                BindLawyerData();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_DeleteLawyer");
            sqlCmd.Parameters.AddWithValue("@LawyerId", this.lawyerId);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {

                MessageBox.Show("ข้อมูลถูกลบแล้ว.");

                ClearForm();

                BindLawyerData();
            }
        }

        private void dataGridViewLawyer_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewLawyer.Columns[e.ColumnIndex].Name == "Select")
            {
                this.lawyerId = dataGridViewLawyer.Rows[e.RowIndex].Cells["Id"].Value.ToString();

                textBoxLawyerName.Text = dataGridViewLawyer.Rows[e.RowIndex].Cells["LawyerName"].Value.ToString();

                btnAdd.Enabled = false;
                btnModify.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void dataGridViewLawyer_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dataGridViewLawyer.Rows[e.RowIndex].Cells["RecordNo"].Value = e.RowIndex + 1;
            }
        }
    }
}
