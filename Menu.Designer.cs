﻿namespace Lawyer
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCourt = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemAllegationType = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemProsecutor = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemComplainant = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDefendant = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemLawyer = new System.Windows.Forms.ToolStripMenuItem();
            this.งานทนายToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemComplaninantWork = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemCaseWork = new System.Windows.Forms.ToolStripMenuItem();
            this.แจงเตอนToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemWorkDetail = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.PeachPuff;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem,
            this.งานทนายToolStripMenuItem,
            this.แจงเตอนToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1064, 29);
            this.menuStrip1.TabIndex = 79;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuItem
            // 
            this.menuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCourt,
            this.menuItemAllegationType,
            this.menuItemProsecutor,
            this.menuItemComplainant,
            this.menuItemDefendant,
            this.menuItemLawyer});
            this.menuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuItem.Name = "menuItem";
            this.menuItem.Size = new System.Drawing.Size(100, 25);
            this.menuItem.Text = "1. ข้อมูลหลัก";
            // 
            // menuCourt
            // 
            this.menuCourt.Name = "menuCourt";
            this.menuCourt.Size = new System.Drawing.Size(152, 26);
            this.menuCourt.Text = "ศาล";
            this.menuCourt.Click += new System.EventHandler(this.menuCourt_Click);
            // 
            // menuItemAllegationType
            // 
            this.menuItemAllegationType.Name = "menuItemAllegationType";
            this.menuItemAllegationType.Size = new System.Drawing.Size(152, 26);
            this.menuItemAllegationType.Text = "ข้อหา";
            this.menuItemAllegationType.Click += new System.EventHandler(this.menuItemAllegationType_Click);
            // 
            // menuItemProsecutor
            // 
            this.menuItemProsecutor.Name = "menuItemProsecutor";
            this.menuItemProsecutor.Size = new System.Drawing.Size(152, 26);
            this.menuItemProsecutor.Text = "โจทก์";
            this.menuItemProsecutor.Click += new System.EventHandler(this.menuItemProsecutor_Click);
            // 
            // menuItemComplainant
            // 
            this.menuItemComplainant.Name = "menuItemComplainant";
            this.menuItemComplainant.Size = new System.Drawing.Size(152, 26);
            this.menuItemComplainant.Text = "ผู้ร้อง";
            this.menuItemComplainant.Click += new System.EventHandler(this.menuItemComplainant_Click);
            // 
            // menuItemDefendant
            // 
            this.menuItemDefendant.Name = "menuItemDefendant";
            this.menuItemDefendant.Size = new System.Drawing.Size(152, 26);
            this.menuItemDefendant.Text = "จำเลย";
            this.menuItemDefendant.Click += new System.EventHandler(this.menuItemDefendant_Click);
            // 
            // menuItemLawyer
            // 
            this.menuItemLawyer.Name = "menuItemLawyer";
            this.menuItemLawyer.Size = new System.Drawing.Size(152, 26);
            this.menuItemLawyer.Text = "ทนายความ";
            this.menuItemLawyer.Click += new System.EventHandler(this.menuItemLawyer_Click);
            // 
            // งานทนายToolStripMenuItem
            // 
            this.งานทนายToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemComplaninantWork,
            this.menuItemCaseWork});
            this.งานทนายToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.งานทนายToolStripMenuItem.Name = "งานทนายToolStripMenuItem";
            this.งานทนายToolStripMenuItem.Size = new System.Drawing.Size(96, 25);
            this.งานทนายToolStripMenuItem.Text = "2. งานทนาย";
            // 
            // menuItemComplaninantWork
            // 
            this.menuItemComplaninantWork.Name = "menuItemComplaninantWork";
            this.menuItemComplaninantWork.Size = new System.Drawing.Size(155, 26);
            this.menuItemComplaninantWork.Text = "งานฟ้อง";
            this.menuItemComplaninantWork.Click += new System.EventHandler(this.menuItemComplaninantWork_Click);
            // 
            // menuItemCaseWork
            // 
            this.menuItemCaseWork.Name = "menuItemCaseWork";
            this.menuItemCaseWork.Size = new System.Drawing.Size(155, 26);
            this.menuItemCaseWork.Text = "งานบังคับคดี";
            this.menuItemCaseWork.Click += new System.EventHandler(this.menuItemCaseWork_Click);
            // 
            // แจงเตอนToolStripMenuItem
            // 
            this.แจงเตอนToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemWorkDetail});
            this.แจงเตอนToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.แจงเตอนToolStripMenuItem.Name = "แจงเตอนToolStripMenuItem";
            this.แจงเตอนToolStripMenuItem.Size = new System.Drawing.Size(95, 25);
            this.แจงเตอนToolStripMenuItem.Text = "3. แจ้งเตือน";
            // 
            // menuItemWorkDetail
            // 
            this.menuItemWorkDetail.Name = "menuItemWorkDetail";
            this.menuItemWorkDetail.Size = new System.Drawing.Size(138, 26);
            this.menuItemWorkDetail.Text = "ข้อมูลงาน";
            this.menuItemWorkDetail.Click += new System.EventHandler(this.menuItemWorkDetail_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 546);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1064, 22);
            this.statusStrip1.TabIndex = 80;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Lawyer.Properties.Resources.Logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1064, 568);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Menu";
            this.Text = "บริษัท ศราภูมิ ลีเกิล คอนซัลแท็นต์ จำกัด";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuItem;
        private System.Windows.Forms.ToolStripMenuItem menuCourt;
        private System.Windows.Forms.ToolStripMenuItem menuItemAllegationType;
        private System.Windows.Forms.ToolStripMenuItem menuItemProsecutor;
        private System.Windows.Forms.ToolStripMenuItem menuItemComplainant;
        private System.Windows.Forms.ToolStripMenuItem menuItemDefendant;
        private System.Windows.Forms.ToolStripMenuItem menuItemLawyer;
        private System.Windows.Forms.ToolStripMenuItem แจงเตอนToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItemWorkDetail;
        private System.Windows.Forms.ToolStripMenuItem งานทนายToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItemComplaninantWork;
        private System.Windows.Forms.ToolStripMenuItem menuItemCaseWork;
        private System.Windows.Forms.StatusStrip statusStrip1;
    }
}