﻿namespace Lawyer
{
    partial class Prosecutor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxProsecutorName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridViewProsecutor = new System.Windows.Forms.DataGridView();
            this.textBoxProsecutorType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Select = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProsecutorType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProsecutorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProsecutor)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.ForeColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(274, 79);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 34;
            this.btnDelete.Text = "ลบ";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.Enabled = false;
            this.btnModify.ForeColor = System.Drawing.Color.Blue;
            this.btnModify.Location = new System.Drawing.Point(192, 79);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 32;
            this.btnModify.Text = "แก้ไข";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAdd.Location = new System.Drawing.Point(111, 79);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 31;
            this.btnAdd.Text = "เพิ่ม";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gold;
            this.panel1.Location = new System.Drawing.Point(-141, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 48);
            this.panel1.TabIndex = 35;
            // 
            // textBoxProsecutorName
            // 
            this.textBoxProsecutorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxProsecutorName.Location = new System.Drawing.Point(86, 37);
            this.textBoxProsecutorName.Name = "textBoxProsecutorName";
            this.textBoxProsecutorName.Size = new System.Drawing.Size(460, 22);
            this.textBoxProsecutorName.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(41, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 16);
            this.label4.TabIndex = 26;
            this.label4.Text = "โจทก์";
            // 
            // dataGridViewProsecutor
            // 
            this.dataGridViewProsecutor.AllowUserToAddRows = false;
            this.dataGridViewProsecutor.AllowUserToDeleteRows = false;
            this.dataGridViewProsecutor.AllowUserToResizeRows = false;
            this.dataGridViewProsecutor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProsecutor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Select,
            this.Id,
            this.RecordNo,
            this.ProsecutorType,
            this.ProsecutorName});
            this.dataGridViewProsecutor.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewProsecutor.Location = new System.Drawing.Point(36, 147);
            this.dataGridViewProsecutor.Name = "dataGridViewProsecutor";
            this.dataGridViewProsecutor.Size = new System.Drawing.Size(509, 150);
            this.dataGridViewProsecutor.TabIndex = 36;
            this.dataGridViewProsecutor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProsecutor_CellContentClick);
            this.dataGridViewProsecutor.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewProsecutor_CellFormatting);
            // 
            // textBoxProsecutorType
            // 
            this.textBoxProsecutorType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxProsecutorType.Location = new System.Drawing.Point(84, 12);
            this.textBoxProsecutorType.Name = "textBoxProsecutorType";
            this.textBoxProsecutorType.Size = new System.Drawing.Size(72, 22);
            this.textBoxProsecutorType.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 16);
            this.label1.TabIndex = 37;
            this.label1.Text = "ประเภทโจทก์";
            // 
            // Select
            // 
            this.Select.DataPropertyName = "ProsecutorType";
            this.Select.HeaderText = "เลือก";
            this.Select.Name = "Select";
            this.Select.Text = "เลือก";
            this.Select.ToolTipText = "เลือก";
            this.Select.UseColumnTextForLinkValue = true;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "ProsecutorId";
            this.Id.HeaderText = "ProsecutorId";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // RecordNo
            // 
            this.RecordNo.HeaderText = "No.";
            this.RecordNo.Name = "RecordNo";
            // 
            // ProsecutorType
            // 
            this.ProsecutorType.DataPropertyName = "ProsecutorType";
            this.ProsecutorType.HeaderText = "ประเภทโจทก์";
            this.ProsecutorType.Name = "ProsecutorType";
            // 
            // ProsecutorName
            // 
            this.ProsecutorName.DataPropertyName = "ProsecutorName";
            this.ProsecutorName.HeaderText = "โจทก์";
            this.ProsecutorName.Name = "ProsecutorName";
            // 
            // Prosecutor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 444);
            this.Controls.Add(this.textBoxProsecutorType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewProsecutor);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBoxProsecutorName);
            this.Controls.Add(this.label4);
            this.Name = "Prosecutor";
            this.Text = "โจทย์";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProsecutor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxProsecutorName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridViewProsecutor;
        private System.Windows.Forms.TextBox textBoxProsecutorType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewLinkColumn Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProsecutorType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProsecutorName;
    }
}