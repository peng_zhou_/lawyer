﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace Lawyer
{
    class Database
    {
        SqlConnection sqlCon = new SqlConnection();

        public Database()
        {
            sqlCon.ConnectionString = ConfigurationManager.ConnectionStrings["connectionStringLocal"].ToString();
        }

        public SqlConnection GetSqlConnection()
        {
            return sqlCon;
        }

        public string TestConnection()
        {
            try
            {
                sqlCon.Open();
                sqlCon.Close();
            }

            catch (SqlException ex)
            {
                System.Windows.Forms.MessageBox.Show("เชื่อมฐานข้อมูลไม่สำเร็จ กรุณาติดต่อผู้ดูแลระบบ", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return "success";
        }

        public DataSet GetDataSet(SqlCommand sqlCmd)
        {
            DataSet ds = new DataSet();

            try
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Connection = sqlCon;

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(ds);
            }

            catch (SqlException ex)
            {
                System.Windows.Forms.MessageBox.Show("เกิดข้อผิดพลาดในขณะเรียกดูข้อมูล กรุณาติดต่อผู้ดูแลระบบ", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return ds;
        }

        public DataTable GetDataTable(string spName)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand sqlCmd = new SqlCommand(spName);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Connection = sqlCon;

                SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);

                sqlDa.Fill(dt);
            }

            catch (SqlException ex)
            {
                System.Windows.Forms.MessageBox.Show("เกิดข้อผิดพลาดในขณะเรียกดูข้อมูล กรุณาติดต่อผู้ดูแลระบบ", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return dt;
        }

        public DataTable GetDataTable(SqlCommand sqlCmd)
        {
            DataSet ds = GetDataSet(sqlCmd);
            DataTable dt;

            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            else
            {
                dt = new DataTable();
            }

            return dt;
        }

        public DataRow GetDataRow(SqlCommand sqlCmd)
        {
            DataTable dt = GetDataTable(sqlCmd);
            DataRow dr = null;

            if (dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];
            }

            return dr;
        }

        public object ExecuteScalar(SqlCommand sqlCmd)
        {
            object scalar = null;

            try
            {
                sqlCmd.Connection = sqlCon;
                sqlCon.Open();
                scalar = sqlCmd.ExecuteScalar();
                sqlCon.Close();
            }

            catch (SqlException ex)
            {
                sqlCon.Close();
                //throw;
                System.Windows.Forms.MessageBox.Show("เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return scalar;
        }

        public int ExecuteNonQuery(SqlCommand sqlCmd)
        {
            int rowAffected = 0;

            try
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Connection = sqlCon;
                sqlCon.Open();
                rowAffected = sqlCmd.ExecuteNonQuery();
                sqlCon.Close();
            }

            catch (SqlException ex)
            {
                sqlCon.Close();
                //throw;
                //System.Windows.Forms.MessageBox.Show(ex.Message);
                System.Windows.Forms.MessageBox.Show("เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return rowAffected;
        }

        public int ExecuteTransactionNonQuery(SqlCommand sqlCmd, SqlTransaction sqlTrans)
        {
            int rowAffected = 0;

            try
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Connection = sqlCon;
                rowAffected = sqlCmd.ExecuteNonQuery();
            }

            catch (SqlException ex)
            {
                sqlTrans.Rollback();
                sqlCon.Close();
                //throw;
                //System.Windows.Forms.MessageBox.Show(ex.Message);
                System.Windows.Forms.MessageBox.Show("เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }

            return rowAffected;
        }
    }
}
