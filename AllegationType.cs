﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lawyer
{
    public partial class formAllegationType : Form
    {
        Database database = new Database();
        string allegationTypeId = "0";

        public formAllegationType()
        {
            InitializeComponent();

            dataGridViewAllegationType.AutoGenerateColumns = false;
            BindAllegationTypeData();
        }

        private void BindAllegationTypeData()
        {
            SqlCommand sqlCmd = new SqlCommand("USP_GetAllegationTypeData");
            dataGridViewAllegationType.DataSource = database.GetDataTable(sqlCmd);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_InsertAllegationType");
            sqlCmd.Parameters.AddWithValue("@AllegationType", textBoxAllegationType.Text);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกเพิ่มแล้ว.");

                BindAllegationTypeData();
            }
        }
        private void ClearForm()
        {
            this.allegationTypeId = "0";
            textBoxAllegationType.Text = "";

            btnAdd.Enabled = true;
            btnModify.Enabled = false;
            btnDelete.Enabled = false;
        }
        private void btnModify_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_UpdateAllegationType");
            sqlCmd.Parameters.AddWithValue("@AllegationTypeId", this.allegationTypeId);
            sqlCmd.Parameters.AddWithValue("@AllegationType", textBoxAllegationType.Text);

            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกแก้ไขแล้ว.");

                ClearForm();

                BindAllegationTypeData();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_DeleteAllegationType");
            sqlCmd.Parameters.AddWithValue("@AllegationTypeId", this.allegationTypeId);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {

                MessageBox.Show("ข้อมูลถูกลบแล้ว.");

                ClearForm();

                BindAllegationTypeData();
            }
        }

        private void dataGridViewAllegationType_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dataGridViewAllegationType.Rows[e.RowIndex].Cells["RecordNo"].Value = e.RowIndex + 1;
            }
        }

        private void dataGridViewAllegationType_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewAllegationType.Columns[e.ColumnIndex].Name == "Select")
            {
                this.allegationTypeId = dataGridViewAllegationType.Rows[e.RowIndex].Cells["AllegationTypeId1"].Value.ToString();

                textBoxAllegationType.Text = dataGridViewAllegationType.Rows[e.RowIndex].Cells["AllegationType"].Value.ToString();

                btnAdd.Enabled = false;
                btnModify.Enabled = true;
                btnDelete.Enabled = true;
            }
        }
    }
}
