﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lawyer
{
    public partial class Court : Form
    {
        Database database = new Database();
        string courtId = "0";

        public Court()
        {
            InitializeComponent();

            dataGridViewCourt.AutoGenerateColumns = false;
            BindCourtData();
        }

        private void BindCourtData()
        {
            SqlCommand sqlCmd1 = new SqlCommand("USP_GetCourtData");
            dataGridViewCourt.DataSource = database.GetDataTable(sqlCmd1);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_InsertCourt");
            sqlCmd.Parameters.AddWithValue("@CourtName", textBoxCourtName.Text);
            sqlCmd.Parameters.AddWithValue("@CourtLocation", textBoxCourtLocation.Text);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกเพิ่มแล้ว.");

                BindCourtData();
            }
        }

        private void dataGridViewCourt_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dataGridViewCourt.Rows[e.RowIndex].Cells["RecordNo"].Value = e.RowIndex + 1;
            }
        }

        private void dataGridViewCourt_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewCourt.Columns[e.ColumnIndex].Name == "Select")
            {
                courtId = dataGridViewCourt.Rows[e.RowIndex].Cells["CourtId1"].Value.ToString();

                textBoxCourtName.Text = dataGridViewCourt.Rows[e.RowIndex].Cells["CourtName"].Value.ToString();
                textBoxCourtLocation.Text = dataGridViewCourt.Rows[e.RowIndex].Cells["CourtLocation"].Value.ToString();

                btnAdd.Enabled = false;
                btnModify.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void ClearForm()
        {
            this.courtId = "0";
            textBoxCourtName.Text = "";
            textBoxCourtLocation.Text = "";

            btnAdd.Enabled = true;
            btnModify.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_UpdateCourt");
            sqlCmd.Parameters.AddWithValue("@CourtId", this.courtId);
            sqlCmd.Parameters.AddWithValue("@CourtName", textBoxCourtName.Text);
            sqlCmd.Parameters.AddWithValue("@CourtLocation", textBoxCourtLocation.Text);

            if(database.ExecuteNonQuery(sqlCmd) > 0)
            { 
                MessageBox.Show("ข้อมูลถูกแก้ไขแล้ว.");

                ClearForm();

                BindCourtData();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_DeleteCourt");
            sqlCmd.Parameters.AddWithValue("@CourtId", this.courtId);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {

                MessageBox.Show("ข้อมูลถูกลบแล้ว.");

                ClearForm();

                BindCourtData();
            }
        }
    }
}
