﻿namespace Lawyer
{
    partial class Court
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxCourtName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCourtLocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewCourt = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Select = new System.Windows.Forms.DataGridViewLinkColumn();
            this.CourtId1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourtName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourtLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourt)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxCourtName
            // 
            this.textBoxCourtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCourtName.Location = new System.Drawing.Point(70, 12);
            this.textBoxCourtName.Name = "textBoxCourtName";
            this.textBoxCourtName.Size = new System.Drawing.Size(460, 22);
            this.textBoxCourtName.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "ชื่อศาล";
            // 
            // textBoxCourtLocation
            // 
            this.textBoxCourtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxCourtLocation.Location = new System.Drawing.Point(70, 40);
            this.textBoxCourtLocation.Name = "textBoxCourtLocation";
            this.textBoxCourtLocation.Size = new System.Drawing.Size(460, 22);
            this.textBoxCourtLocation.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 16);
            this.label1.TabIndex = 12;
            this.label1.Text = "ที่อยู่ศาล";
            // 
            // dataGridViewCourt
            // 
            this.dataGridViewCourt.AllowUserToAddRows = false;
            this.dataGridViewCourt.AllowUserToDeleteRows = false;
            this.dataGridViewCourt.AllowUserToResizeRows = false;
            this.dataGridViewCourt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCourt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Select,
            this.CourtId1,
            this.RecordNo,
            this.CourtName,
            this.CourtLocation});
            this.dataGridViewCourt.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewCourt.Location = new System.Drawing.Point(12, 121);
            this.dataGridViewCourt.Name = "dataGridViewCourt";
            this.dataGridViewCourt.Size = new System.Drawing.Size(509, 150);
            this.dataGridViewCourt.TabIndex = 14;
            this.dataGridViewCourt.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCourt_CellContentClick);
            this.dataGridViewCourt.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewCourt_CellFormatting);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.ForeColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(258, 79);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 24;
            this.btnDelete.Text = "ลบ";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.ForeColor = System.Drawing.Color.Orange;
            this.btnCancel.Location = new System.Drawing.Point(343, 79);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 23;
            this.btnCancel.Text = "เคลียร์";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            // 
            // btnModify
            // 
            this.btnModify.Enabled = false;
            this.btnModify.ForeColor = System.Drawing.Color.Blue;
            this.btnModify.Location = new System.Drawing.Point(176, 79);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 22;
            this.btnModify.Text = "แก้ไข";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAdd.Location = new System.Drawing.Point(95, 79);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 21;
            this.btnAdd.Text = "เพิ่ม";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gold;
            this.panel1.Location = new System.Drawing.Point(-157, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 48);
            this.panel1.TabIndex = 25;
            // 
            // Select
            // 
            this.Select.HeaderText = "เลือก";
            this.Select.Name = "Select";
            this.Select.Text = "เลือก";
            this.Select.ToolTipText = "เลือก";
            this.Select.UseColumnTextForLinkValue = true;
            // 
            // CourtId1
            // 
            this.CourtId1.DataPropertyName = "CourtId";
            this.CourtId1.HeaderText = "Column1";
            this.CourtId1.Name = "CourtId1";
            this.CourtId1.Visible = false;
            // 
            // RecordNo
            // 
            this.RecordNo.HeaderText = "No.";
            this.RecordNo.Name = "RecordNo";
            // 
            // CourtName
            // 
            this.CourtName.DataPropertyName = "CourtName";
            this.CourtName.HeaderText = "ชื่อศาล";
            this.CourtName.Name = "CourtName";
            // 
            // CourtLocation
            // 
            this.CourtLocation.DataPropertyName = "CourtLocation";
            this.CourtLocation.HeaderText = "ที่อยู่ศาล";
            this.CourtLocation.Name = "CourtLocation";
            // 
            // Court
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 423);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridViewCourt);
            this.Controls.Add(this.textBoxCourtLocation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxCourtName);
            this.Controls.Add(this.label4);
            this.Name = "Court";
            this.Text = "ศาล";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxCourtName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCourtLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewCourt;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewLinkColumn Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourtId1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourtName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourtLocation;
    }
}