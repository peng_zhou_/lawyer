﻿namespace Lawyer
{
    partial class Lawyer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewLawyer = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxLawyerName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxLawyerType = new System.Windows.Forms.ComboBox();
            this.Select = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LawyerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLawyer)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewLawyer
            // 
            this.dataGridViewLawyer.AllowUserToAddRows = false;
            this.dataGridViewLawyer.AllowUserToDeleteRows = false;
            this.dataGridViewLawyer.AllowUserToResizeRows = false;
            this.dataGridViewLawyer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLawyer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Select,
            this.Id,
            this.RecordNo,
            this.LawyerName});
            this.dataGridViewLawyer.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewLawyer.Location = new System.Drawing.Point(1, 147);
            this.dataGridViewLawyer.Name = "dataGridViewLawyer";
            this.dataGridViewLawyer.Size = new System.Drawing.Size(509, 150);
            this.dataGridViewLawyer.TabIndex = 57;
            this.dataGridViewLawyer.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLawyer_CellContentClick);
            this.dataGridViewLawyer.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewLawyer_CellFormatting);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.ForeColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(239, 79);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 55;
            this.btnDelete.Text = "ลบ";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.Enabled = false;
            this.btnModify.ForeColor = System.Drawing.Color.Blue;
            this.btnModify.Location = new System.Drawing.Point(157, 79);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 23);
            this.btnModify.TabIndex = 54;
            this.btnModify.Text = "แก้ไข";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAdd.Location = new System.Drawing.Point(76, 79);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 53;
            this.btnAdd.Text = "เพิ่ม";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gold;
            this.panel1.Location = new System.Drawing.Point(-176, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(794, 48);
            this.panel1.TabIndex = 56;
            // 
            // textBoxLawyerName
            // 
            this.textBoxLawyerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBoxLawyerName.Location = new System.Drawing.Point(83, 34);
            this.textBoxLawyerName.Name = "textBoxLawyerName";
            this.textBoxLawyerName.Size = new System.Drawing.Size(428, 22);
            this.textBoxLawyerName.TabIndex = 52;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 16);
            this.label4.TabIndex = 51;
            this.label4.Text = "ทนาย";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 58;
            this.label1.Text = "ประเภททนาย";
            // 
            // comboBoxLawyerType
            // 
            this.comboBoxLawyerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLawyerType.FormattingEnabled = true;
            this.comboBoxLawyerType.Items.AddRange(new object[] {
            "ทนายฟ้อง",
            "ทนายความสืบฯ",
            "ทนายความหลังพิพากษา"});
            this.comboBoxLawyerType.Location = new System.Drawing.Point(84, 5);
            this.comboBoxLawyerType.Name = "comboBoxLawyerType";
            this.comboBoxLawyerType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxLawyerType.TabIndex = 59;
            // 
            // Select
            // 
            this.Select.HeaderText = "เลือก";
            this.Select.Name = "Select";
            this.Select.Text = "เลือก";
            this.Select.ToolTipText = "เลือก";
            this.Select.UseColumnTextForLinkValue = true;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "LawyerId";
            this.Id.HeaderText = "DefendantId";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // RecordNo
            // 
            this.RecordNo.HeaderText = "No.";
            this.RecordNo.Name = "RecordNo";
            // 
            // LawyerName
            // 
            this.LawyerName.DataPropertyName = "LawyerName";
            this.LawyerName.HeaderText = "ทนาย";
            this.LawyerName.Name = "LawyerName";
            // 
            // Lawyer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 431);
            this.Controls.Add(this.comboBoxLawyerType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewLawyer);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBoxLawyerName);
            this.Controls.Add(this.label4);
            this.Name = "Lawyer";
            this.Text = "ทนายความ";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLawyer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewLawyer;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxLawyerName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxLawyerType;
        private System.Windows.Forms.DataGridViewLinkColumn Select;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn LawyerName;
    }
}