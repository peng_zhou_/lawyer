﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lawyer
{
    public partial class AlertWorkDetail : Form
    {
        Database database = new Database();

        public AlertWorkDetail()
        {
            InitializeComponent();

            dataGridViewAlert.AutoGenerateColumns = false;
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_SearchAlert");
            sqlCmd.Parameters.AddWithValue("@AlertType", 1); //comboBoxAlertType.Text);
            sqlCmd.Parameters.AddWithValue("@DateFrom", dateTimePickerFrom.Value);
            sqlCmd.Parameters.AddWithValue("@DateTo", dateTimePickerTo.Value);
            DataTable dt = database.GetDataTable(sqlCmd);
            dataGridViewAlert.DataSource = dt;
        }
    }
}
