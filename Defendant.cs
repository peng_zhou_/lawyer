﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lawyer
{
    public partial class Defendant : Form
    {
        Database database = new Database();
        string defendantId = "0";

        public Defendant()
        {
            InitializeComponent();

            dataGridViewDefendant.AutoGenerateColumns = false;
            BindDefendantData();
        }

        private void BindDefendantData()
        {
            SqlCommand sqlCmd = new SqlCommand("USP_GetDefendantData");
            dataGridViewDefendant.DataSource = database.GetDataTable(sqlCmd);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_InsertDefendant");
            sqlCmd.Parameters.AddWithValue("@DefendantName", textBoxDefendantName.Text);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกเพิ่มแล้ว.");

                BindDefendantData();
            }
        }
        private void ClearForm()
        {
            this.defendantId = "0";
            textBoxDefendantName.Text = "";

            btnAdd.Enabled = true;
            btnModify.Enabled = false;
            btnDelete.Enabled = false;
        }
        private void btnModify_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_UpdateDefendant");
            sqlCmd.Parameters.AddWithValue("@DefendantId", this.defendantId);
            sqlCmd.Parameters.AddWithValue("@DefendantName", textBoxDefendantName.Text);

            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {
                MessageBox.Show("ข้อมูลถูกแก้ไขแล้ว.");

                ClearForm();

                BindDefendantData();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlCommand sqlCmd = new SqlCommand("USP_DeleteDefendant");
            sqlCmd.Parameters.AddWithValue("@DefendantId", this.defendantId);
            if (database.ExecuteNonQuery(sqlCmd) > 0)
            {

                MessageBox.Show("ข้อมูลถูกลบแล้ว.");

                ClearForm();

                BindDefendantData();
            }
        }

        private void dataGridViewDefendant_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewDefendant.Columns[e.ColumnIndex].Name == "Select")
            {
                this.defendantId = dataGridViewDefendant.Rows[e.RowIndex].Cells["Id"].Value.ToString();

                textBoxDefendantName.Text = dataGridViewDefendant.Rows[e.RowIndex].Cells["DefendantName"].Value.ToString();

                btnAdd.Enabled = false;
                btnModify.Enabled = true;
                btnDelete.Enabled = true;
            }
        }

        private void dataGridViewDefendant_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                dataGridViewDefendant.Rows[e.RowIndex].Cells["RecordNo"].Value = e.RowIndex + 1;
            }
        }
    }
}
